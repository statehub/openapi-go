/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// CreateStateLocationAzure struct for CreateStateLocationAzure
type CreateStateLocationAzure struct {
	// An Azure Region
	Region string `json:"region"`
}

// NewCreateStateLocationAzure instantiates a new CreateStateLocationAzure object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateStateLocationAzure(region string) *CreateStateLocationAzure {
	this := CreateStateLocationAzure{}
	this.Region = region
	return &this
}

// NewCreateStateLocationAzureWithDefaults instantiates a new CreateStateLocationAzure object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateStateLocationAzureWithDefaults() *CreateStateLocationAzure {
	this := CreateStateLocationAzure{}
	return &this
}

// GetRegion returns the Region field value
func (o *CreateStateLocationAzure) GetRegion() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Region
}

// GetRegionOk returns a tuple with the Region field value
// and a boolean to check if the value has been set.
func (o *CreateStateLocationAzure) GetRegionOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Region, true
}

// SetRegion sets field value
func (o *CreateStateLocationAzure) SetRegion(v string) {
	o.Region = v
}

func (o CreateStateLocationAzure) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["region"] = o.Region
	}
	return json.Marshal(toSerialize)
}

type NullableCreateStateLocationAzure struct {
	value *CreateStateLocationAzure
	isSet bool
}

func (v NullableCreateStateLocationAzure) Get() *CreateStateLocationAzure {
	return v.value
}

func (v *NullableCreateStateLocationAzure) Set(val *CreateStateLocationAzure) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateStateLocationAzure) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateStateLocationAzure) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateStateLocationAzure(val *CreateStateLocationAzure) *NullableCreateStateLocationAzure {
	return &NullableCreateStateLocationAzure{value: val, isSet: true}
}

func (v NullableCreateStateLocationAzure) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateStateLocationAzure) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


