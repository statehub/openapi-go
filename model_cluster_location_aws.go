/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: v0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ClusterLocationAws struct for ClusterLocationAws
type ClusterLocationAws struct {
	// An AWS Region
	Region string `json:"region"`
	// AWS Account Principal
	AccountPrincipal *string `json:"accountPrincipal,omitempty"`
}

// NewClusterLocationAws instantiates a new ClusterLocationAws object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewClusterLocationAws(region string) *ClusterLocationAws {
	this := ClusterLocationAws{}
	this.Region = region
	return &this
}

// NewClusterLocationAwsWithDefaults instantiates a new ClusterLocationAws object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewClusterLocationAwsWithDefaults() *ClusterLocationAws {
	this := ClusterLocationAws{}
	return &this
}

// GetRegion returns the Region field value
func (o *ClusterLocationAws) GetRegion() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Region
}

// GetRegionOk returns a tuple with the Region field value
// and a boolean to check if the value has been set.
func (o *ClusterLocationAws) GetRegionOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Region, true
}

// SetRegion sets field value
func (o *ClusterLocationAws) SetRegion(v string) {
	o.Region = v
}

// GetAccountPrincipal returns the AccountPrincipal field value if set, zero value otherwise.
func (o *ClusterLocationAws) GetAccountPrincipal() string {
	if o == nil || o.AccountPrincipal == nil {
		var ret string
		return ret
	}
	return *o.AccountPrincipal
}

// GetAccountPrincipalOk returns a tuple with the AccountPrincipal field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ClusterLocationAws) GetAccountPrincipalOk() (*string, bool) {
	if o == nil || o.AccountPrincipal == nil {
		return nil, false
	}
	return o.AccountPrincipal, true
}

// HasAccountPrincipal returns a boolean if a field has been set.
func (o *ClusterLocationAws) HasAccountPrincipal() bool {
	if o != nil && o.AccountPrincipal != nil {
		return true
	}

	return false
}

// SetAccountPrincipal gets a reference to the given string and assigns it to the AccountPrincipal field.
func (o *ClusterLocationAws) SetAccountPrincipal(v string) {
	o.AccountPrincipal = &v
}

func (o ClusterLocationAws) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["region"] = o.Region
	}
	if o.AccountPrincipal != nil {
		toSerialize["accountPrincipal"] = o.AccountPrincipal
	}
	return json.Marshal(toSerialize)
}

type NullableClusterLocationAws struct {
	value *ClusterLocationAws
	isSet bool
}

func (v NullableClusterLocationAws) Get() *ClusterLocationAws {
	return v.value
}

func (v *NullableClusterLocationAws) Set(val *ClusterLocationAws) {
	v.value = val
	v.isSet = true
}

func (v NullableClusterLocationAws) IsSet() bool {
	return v.isSet
}

func (v *NullableClusterLocationAws) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableClusterLocationAws(val *ClusterLocationAws) *NullableClusterLocationAws {
	return &NullableClusterLocationAws{value: val, isSet: true}
}

func (v NullableClusterLocationAws) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableClusterLocationAws) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


