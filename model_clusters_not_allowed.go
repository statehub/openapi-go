/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ClustersNotAllowed struct for ClustersNotAllowed
type ClustersNotAllowed struct {
	ErrorCode string `json:"errorCode"`
}

// NewClustersNotAllowed instantiates a new ClustersNotAllowed object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewClustersNotAllowed(errorCode string) *ClustersNotAllowed {
	this := ClustersNotAllowed{}
	this.ErrorCode = errorCode
	return &this
}

// NewClustersNotAllowedWithDefaults instantiates a new ClustersNotAllowed object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewClustersNotAllowedWithDefaults() *ClustersNotAllowed {
	this := ClustersNotAllowed{}
	return &this
}

// GetErrorCode returns the ErrorCode field value
func (o *ClustersNotAllowed) GetErrorCode() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ErrorCode
}

// GetErrorCodeOk returns a tuple with the ErrorCode field value
// and a boolean to check if the value has been set.
func (o *ClustersNotAllowed) GetErrorCodeOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ErrorCode, true
}

// SetErrorCode sets field value
func (o *ClustersNotAllowed) SetErrorCode(v string) {
	o.ErrorCode = v
}

func (o ClustersNotAllowed) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["errorCode"] = o.ErrorCode
	}
	return json.Marshal(toSerialize)
}

type NullableClustersNotAllowed struct {
	value *ClustersNotAllowed
	isSet bool
}

func (v NullableClustersNotAllowed) Get() *ClustersNotAllowed {
	return v.value
}

func (v *NullableClustersNotAllowed) Set(val *ClustersNotAllowed) {
	v.value = val
	v.isSet = true
}

func (v NullableClustersNotAllowed) IsSet() bool {
	return v.isSet
}

func (v *NullableClustersNotAllowed) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableClustersNotAllowed(val *ClustersNotAllowed) *NullableClustersNotAllowed {
	return &NullableClustersNotAllowed{value: val, isSet: true}
}

func (v NullableClustersNotAllowed) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableClustersNotAllowed) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


