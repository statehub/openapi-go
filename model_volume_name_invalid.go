/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// VolumeNameInvalid struct for VolumeNameInvalid
type VolumeNameInvalid struct {
	ErrorCode string `json:"errorCode"`
	Name string `json:"name"`
}

// NewVolumeNameInvalid instantiates a new VolumeNameInvalid object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVolumeNameInvalid(errorCode string, name string) *VolumeNameInvalid {
	this := VolumeNameInvalid{}
	this.ErrorCode = errorCode
	this.Name = name
	return &this
}

// NewVolumeNameInvalidWithDefaults instantiates a new VolumeNameInvalid object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVolumeNameInvalidWithDefaults() *VolumeNameInvalid {
	this := VolumeNameInvalid{}
	return &this
}

// GetErrorCode returns the ErrorCode field value
func (o *VolumeNameInvalid) GetErrorCode() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ErrorCode
}

// GetErrorCodeOk returns a tuple with the ErrorCode field value
// and a boolean to check if the value has been set.
func (o *VolumeNameInvalid) GetErrorCodeOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ErrorCode, true
}

// SetErrorCode sets field value
func (o *VolumeNameInvalid) SetErrorCode(v string) {
	o.ErrorCode = v
}

// GetName returns the Name field value
func (o *VolumeNameInvalid) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *VolumeNameInvalid) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *VolumeNameInvalid) SetName(v string) {
	o.Name = v
}

func (o VolumeNameInvalid) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["errorCode"] = o.ErrorCode
	}
	if true {
		toSerialize["name"] = o.Name
	}
	return json.Marshal(toSerialize)
}

type NullableVolumeNameInvalid struct {
	value *VolumeNameInvalid
	isSet bool
}

func (v NullableVolumeNameInvalid) Get() *VolumeNameInvalid {
	return v.value
}

func (v *NullableVolumeNameInvalid) Set(val *VolumeNameInvalid) {
	v.value = val
	v.isSet = true
}

func (v NullableVolumeNameInvalid) IsSet() bool {
	return v.isSet
}

func (v *NullableVolumeNameInvalid) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVolumeNameInvalid(val *VolumeNameInvalid) *NullableVolumeNameInvalid {
	return &NullableVolumeNameInvalid{value: val, isSet: true}
}

func (v NullableVolumeNameInvalid) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVolumeNameInvalid) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


