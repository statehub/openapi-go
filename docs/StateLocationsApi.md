# \StateLocationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StateLocationsControllerAddAwsLocation**](StateLocationsApi.md#StateLocationsControllerAddAwsLocation) | **Post** /v0/states/{state}/locations/aws | Add an AWS Location
[**StateLocationsControllerAddAwsWhitelistedPrincipal**](StateLocationsApi.md#StateLocationsControllerAddAwsWhitelistedPrincipal) | **Post** /v0/states/{state}/locations/aws/{region}/whitelist | Add current cluster account principal to whitelist
[**StateLocationsControllerAddAzureLocation**](StateLocationsApi.md#StateLocationsControllerAddAzureLocation) | **Post** /v0/states/{state}/locations/azure | Add an Azure Location
[**StateLocationsControllerApproveAwsPle**](StateLocationsApi.md#StateLocationsControllerApproveAwsPle) | **Put** /v0/states/{state}/locations/aws/{region}/approvedEndpoints/{endpoint} | Approve an AWS private link endpoint
[**StateLocationsControllerApproveAzurePle**](StateLocationsApi.md#StateLocationsControllerApproveAzurePle) | **Put** /v0/states/{state}/locations/azure/{region}/approvedEndpoints/{endpoint} | Approve an Azure private link endpoint
[**StateLocationsControllerDeleteAwsLocation**](StateLocationsApi.md#StateLocationsControllerDeleteAwsLocation) | **Delete** /v0/states/{state}/locations/aws/{region} | Remove an AWS Location
[**StateLocationsControllerDeleteAzureLocation**](StateLocationsApi.md#StateLocationsControllerDeleteAzureLocation) | **Delete** /v0/states/{state}/locations/azure/{region} | Remove an Azure Location
[**StateLocationsControllerGetAwsAccountPrincipals**](StateLocationsApi.md#StateLocationsControllerGetAwsAccountPrincipals) | **Get** /v0/states/{state}/locations/aws/{region}/whitelist | Get AWS whitelisted account principals
[**StateLocationsControllerGetAwsLocation**](StateLocationsApi.md#StateLocationsControllerGetAwsLocation) | **Get** /v0/states/{state}/locations/aws/{region} | Get an AWS Location
[**StateLocationsControllerGetAwsLocations**](StateLocationsApi.md#StateLocationsControllerGetAwsLocations) | **Get** /v0/states/{state}/locations/aws | Get all Aws Location
[**StateLocationsControllerGetAzureLocation**](StateLocationsApi.md#StateLocationsControllerGetAzureLocation) | **Get** /v0/states/{state}/locations/azure/{region} | Get an Azure Location
[**StateLocationsControllerGetAzureLocations**](StateLocationsApi.md#StateLocationsControllerGetAzureLocations) | **Get** /v0/states/{state}/locations/azure | Get all Azure Location
[**StateLocationsControllerGetLocations**](StateLocationsApi.md#StateLocationsControllerGetLocations) | **Get** /v0/states/{state}/locations | Get all Location



## StateLocationsControllerAddAwsLocation

> StateLocationAws StateLocationsControllerAddAwsLocation(ctx, state).StateLocationAwsDto(stateLocationAwsDto).Execute()

Add an AWS Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    stateLocationAwsDto := *openapiclient.NewStateLocationAwsDto("Region_example") // StateLocationAwsDto | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerAddAwsLocation(context.Background(), state).StateLocationAwsDto(stateLocationAwsDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerAddAwsLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerAddAwsLocation`: StateLocationAws
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerAddAwsLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerAddAwsLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **stateLocationAwsDto** | [**StateLocationAwsDto**](StateLocationAwsDto.md) |  | 

### Return type

[**StateLocationAws**](StateLocationAws.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerAddAwsWhitelistedPrincipal

> []string StateLocationsControllerAddAwsWhitelistedPrincipal(ctx, state, region).Execute()

Add current cluster account principal to whitelist

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerAddAwsWhitelistedPrincipal(context.Background(), state, region).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerAddAwsWhitelistedPrincipal``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerAddAwsWhitelistedPrincipal`: []string
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerAddAwsWhitelistedPrincipal`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerAddAwsWhitelistedPrincipalRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**[]string**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerAddAzureLocation

> StateLocationAzure StateLocationsControllerAddAzureLocation(ctx, state).StateLocationAzureDto(stateLocationAzureDto).Execute()

Add an Azure Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    stateLocationAzureDto := *openapiclient.NewStateLocationAzureDto("Region_example") // StateLocationAzureDto | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerAddAzureLocation(context.Background(), state).StateLocationAzureDto(stateLocationAzureDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerAddAzureLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerAddAzureLocation`: StateLocationAzure
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerAddAzureLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerAddAzureLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **stateLocationAzureDto** | [**StateLocationAzureDto**](StateLocationAzureDto.md) |  | 

### Return type

[**StateLocationAzure**](StateLocationAzure.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerApproveAwsPle

> StateLocationsControllerApproveAwsPle(ctx, state, region, endpoint).Execute()

Approve an AWS private link endpoint

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 
    endpoint := "endpoint_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerApproveAwsPle(context.Background(), state, region, endpoint).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerApproveAwsPle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 
**endpoint** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerApproveAwsPleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerApproveAzurePle

> StateLocationsControllerApproveAzurePle(ctx, state, region, endpoint).Execute()

Approve an Azure private link endpoint

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 
    endpoint := "endpoint_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerApproveAzurePle(context.Background(), state, region, endpoint).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerApproveAzurePle``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 
**endpoint** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerApproveAzurePleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerDeleteAwsLocation

> StateLocationAws StateLocationsControllerDeleteAwsLocation(ctx, state, region).Force(force).Execute()

Remove an AWS Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 
    force := true // bool |  (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerDeleteAwsLocation(context.Background(), state, region).Force(force).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerDeleteAwsLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerDeleteAwsLocation`: StateLocationAws
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerDeleteAwsLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerDeleteAwsLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **force** | **bool** |  | [default to false]

### Return type

[**StateLocationAws**](StateLocationAws.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerDeleteAzureLocation

> StateLocationAzure StateLocationsControllerDeleteAzureLocation(ctx, state, region).Force(force).Execute()

Remove an Azure Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 
    force := true // bool |  (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerDeleteAzureLocation(context.Background(), state, region).Force(force).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerDeleteAzureLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerDeleteAzureLocation`: StateLocationAzure
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerDeleteAzureLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerDeleteAzureLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **force** | **bool** |  | [default to false]

### Return type

[**StateLocationAzure**](StateLocationAzure.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetAwsAccountPrincipals

> []string StateLocationsControllerGetAwsAccountPrincipals(ctx, state, region).Execute()

Get AWS whitelisted account principals

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetAwsAccountPrincipals(context.Background(), state, region).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetAwsAccountPrincipals``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetAwsAccountPrincipals`: []string
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetAwsAccountPrincipals`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetAwsAccountPrincipalsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**[]string**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetAwsLocation

> []StateLocationAws StateLocationsControllerGetAwsLocation(ctx, state, region).Execute()

Get an AWS Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetAwsLocation(context.Background(), state, region).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetAwsLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetAwsLocation`: []StateLocationAws
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetAwsLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetAwsLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]StateLocationAws**](StateLocationAws.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetAwsLocations

> []StateLocationAws StateLocationsControllerGetAwsLocations(ctx, state).Execute()

Get all Aws Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetAwsLocations(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetAwsLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetAwsLocations`: []StateLocationAws
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetAwsLocations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetAwsLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]StateLocationAws**](StateLocationAws.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetAzureLocation

> []StateLocationAzure StateLocationsControllerGetAzureLocation(ctx, state, region).Execute()

Get an Azure Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    region := "region_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetAzureLocation(context.Background(), state, region).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetAzureLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetAzureLocation`: []StateLocationAzure
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetAzureLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetAzureLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]StateLocationAzure**](StateLocationAzure.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetAzureLocations

> []StateLocationAzure StateLocationsControllerGetAzureLocations(ctx, state).Execute()

Get all Azure Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetAzureLocations(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetAzureLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetAzureLocations`: []StateLocationAzure
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetAzureLocations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetAzureLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]StateLocationAzure**](StateLocationAzure.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateLocationsControllerGetLocations

> StateLocations StateLocationsControllerGetLocations(ctx, state).Execute()

Get all Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateLocationsApi.StateLocationsControllerGetLocations(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateLocationsApi.StateLocationsControllerGetLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateLocationsControllerGetLocations`: StateLocations
    fmt.Fprintf(os.Stdout, "Response from `StateLocationsApi.StateLocationsControllerGetLocations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateLocationsControllerGetLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**StateLocations**](StateLocations.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

