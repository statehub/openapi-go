# CreateVolumeDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Volume name | 
**SizeGi** | **int32** | The size of the Volume in GiB | 
**FsType** | **string** | The file system type of the Volume | 

## Methods

### NewCreateVolumeDto

`func NewCreateVolumeDto(name string, sizeGi int32, fsType string, ) *CreateVolumeDto`

NewCreateVolumeDto instantiates a new CreateVolumeDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateVolumeDtoWithDefaults

`func NewCreateVolumeDtoWithDefaults() *CreateVolumeDto`

NewCreateVolumeDtoWithDefaults instantiates a new CreateVolumeDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateVolumeDto) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateVolumeDto) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateVolumeDto) SetName(v string)`

SetName sets Name field to given value.


### GetSizeGi

`func (o *CreateVolumeDto) GetSizeGi() int32`

GetSizeGi returns the SizeGi field if non-nil, zero value otherwise.

### GetSizeGiOk

`func (o *CreateVolumeDto) GetSizeGiOk() (*int32, bool)`

GetSizeGiOk returns a tuple with the SizeGi field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSizeGi

`func (o *CreateVolumeDto) SetSizeGi(v int32)`

SetSizeGi sets SizeGi field to given value.


### GetFsType

`func (o *CreateVolumeDto) GetFsType() string`

GetFsType returns the FsType field if non-nil, zero value otherwise.

### GetFsTypeOk

`func (o *CreateVolumeDto) GetFsTypeOk() (*string, bool)`

GetFsTypeOk returns a tuple with the FsType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFsType

`func (o *CreateVolumeDto) SetFsType(v string)`

SetFsType sets FsType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


