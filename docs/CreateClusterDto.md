# CreateClusterDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the Cluster | 
**Namespace** | Pointer to **string** | Cluster namespace  | [optional] 
**Provider** | **string** | Cluster provider | [default to "generic"]
**Locations** | Pointer to [**CreateClusterLocationsDto**](CreateClusterLocationsDto.md) | Cluster locations | [optional] 

## Methods

### NewCreateClusterDto

`func NewCreateClusterDto(name string, provider string, ) *CreateClusterDto`

NewCreateClusterDto instantiates a new CreateClusterDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateClusterDtoWithDefaults

`func NewCreateClusterDtoWithDefaults() *CreateClusterDto`

NewCreateClusterDtoWithDefaults instantiates a new CreateClusterDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateClusterDto) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateClusterDto) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateClusterDto) SetName(v string)`

SetName sets Name field to given value.


### GetNamespace

`func (o *CreateClusterDto) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *CreateClusterDto) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *CreateClusterDto) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.

### HasNamespace

`func (o *CreateClusterDto) HasNamespace() bool`

HasNamespace returns a boolean if a field has been set.

### GetProvider

`func (o *CreateClusterDto) GetProvider() string`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *CreateClusterDto) GetProviderOk() (*string, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *CreateClusterDto) SetProvider(v string)`

SetProvider sets Provider field to given value.


### GetLocations

`func (o *CreateClusterDto) GetLocations() CreateClusterLocationsDto`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *CreateClusterDto) GetLocationsOk() (*CreateClusterLocationsDto, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *CreateClusterDto) SetLocations(v CreateClusterLocationsDto)`

SetLocations sets Locations field to given value.

### HasLocations

`func (o *CreateClusterDto) HasLocations() bool`

HasLocations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


