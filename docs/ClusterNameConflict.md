# ClusterNameConflict

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Cluster** | **string** |  | 

## Methods

### NewClusterNameConflict

`func NewClusterNameConflict(errorCode string, cluster string, ) *ClusterNameConflict`

NewClusterNameConflict instantiates a new ClusterNameConflict object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterNameConflictWithDefaults

`func NewClusterNameConflictWithDefaults() *ClusterNameConflict`

NewClusterNameConflictWithDefaults instantiates a new ClusterNameConflict object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *ClusterNameConflict) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *ClusterNameConflict) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *ClusterNameConflict) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetCluster

`func (o *ClusterNameConflict) GetCluster() string`

GetCluster returns the Cluster field if non-nil, zero value otherwise.

### GetClusterOk

`func (o *ClusterNameConflict) GetClusterOk() (*string, bool)`

GetClusterOk returns a tuple with the Cluster field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCluster

`func (o *ClusterNameConflict) SetCluster(v string)`

SetCluster sets Cluster field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


