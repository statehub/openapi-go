# State

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The unique ID of object | 
**Name** | **string** | The name of the State | 
**StorageClass** | [**StorageClass**](StorageClass.md) | The values to be passed to the StorageClass | 
**Owner** | Pointer to **string** | The Cluster that currently owns the State | [optional] 
**Created** | **time.Time** | Creation date | 
**Modified** | **time.Time** | Modify date | 
**ProvisioningStatus** | **string** | State provisioning status | 
**Locations** | Pointer to [**StateLocations**](StateLocations.md) | The Locations of the State | [optional] 
**Condition** | **string** | State condition | 

## Methods

### NewState

`func NewState(id string, name string, storageClass StorageClass, created time.Time, modified time.Time, provisioningStatus string, condition string, ) *State`

NewState instantiates a new State object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateWithDefaults

`func NewStateWithDefaults() *State`

NewStateWithDefaults instantiates a new State object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *State) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *State) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *State) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *State) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *State) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *State) SetName(v string)`

SetName sets Name field to given value.


### GetStorageClass

`func (o *State) GetStorageClass() StorageClass`

GetStorageClass returns the StorageClass field if non-nil, zero value otherwise.

### GetStorageClassOk

`func (o *State) GetStorageClassOk() (*StorageClass, bool)`

GetStorageClassOk returns a tuple with the StorageClass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageClass

`func (o *State) SetStorageClass(v StorageClass)`

SetStorageClass sets StorageClass field to given value.


### GetOwner

`func (o *State) GetOwner() string`

GetOwner returns the Owner field if non-nil, zero value otherwise.

### GetOwnerOk

`func (o *State) GetOwnerOk() (*string, bool)`

GetOwnerOk returns a tuple with the Owner field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwner

`func (o *State) SetOwner(v string)`

SetOwner sets Owner field to given value.

### HasOwner

`func (o *State) HasOwner() bool`

HasOwner returns a boolean if a field has been set.

### GetCreated

`func (o *State) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *State) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *State) SetCreated(v time.Time)`

SetCreated sets Created field to given value.


### GetModified

`func (o *State) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *State) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *State) SetModified(v time.Time)`

SetModified sets Modified field to given value.


### GetProvisioningStatus

`func (o *State) GetProvisioningStatus() string`

GetProvisioningStatus returns the ProvisioningStatus field if non-nil, zero value otherwise.

### GetProvisioningStatusOk

`func (o *State) GetProvisioningStatusOk() (*string, bool)`

GetProvisioningStatusOk returns a tuple with the ProvisioningStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvisioningStatus

`func (o *State) SetProvisioningStatus(v string)`

SetProvisioningStatus sets ProvisioningStatus field to given value.


### GetLocations

`func (o *State) GetLocations() StateLocations`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *State) GetLocationsOk() (*StateLocations, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *State) SetLocations(v StateLocations)`

SetLocations sets Locations field to given value.

### HasLocations

`func (o *State) HasLocations() bool`

HasLocations returns a boolean if a field has been set.

### GetCondition

`func (o *State) GetCondition() string`

GetCondition returns the Condition field if non-nil, zero value otherwise.

### GetConditionOk

`func (o *State) GetConditionOk() (*string, bool)`

GetConditionOk returns a tuple with the Condition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCondition

`func (o *State) SetCondition(v string)`

SetCondition sets Condition field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


