# NotAuthorized

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Permission** | **string** |  | 
**ResourceType** | **string** |  | 
**ResourceName** | **string** |  | 

## Methods

### NewNotAuthorized

`func NewNotAuthorized(errorCode string, permission string, resourceType string, resourceName string, ) *NotAuthorized`

NewNotAuthorized instantiates a new NotAuthorized object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotAuthorizedWithDefaults

`func NewNotAuthorizedWithDefaults() *NotAuthorized`

NewNotAuthorizedWithDefaults instantiates a new NotAuthorized object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *NotAuthorized) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *NotAuthorized) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *NotAuthorized) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetPermission

`func (o *NotAuthorized) GetPermission() string`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *NotAuthorized) GetPermissionOk() (*string, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *NotAuthorized) SetPermission(v string)`

SetPermission sets Permission field to given value.


### GetResourceType

`func (o *NotAuthorized) GetResourceType() string`

GetResourceType returns the ResourceType field if non-nil, zero value otherwise.

### GetResourceTypeOk

`func (o *NotAuthorized) GetResourceTypeOk() (*string, bool)`

GetResourceTypeOk returns a tuple with the ResourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceType

`func (o *NotAuthorized) SetResourceType(v string)`

SetResourceType sets ResourceType field to given value.


### GetResourceName

`func (o *NotAuthorized) GetResourceName() string`

GetResourceName returns the ResourceName field if non-nil, zero value otherwise.

### GetResourceNameOk

`func (o *NotAuthorized) GetResourceNameOk() (*string, bool)`

GetResourceNameOk returns a tuple with the ResourceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceName

`func (o *NotAuthorized) SetResourceName(v string)`

SetResourceName sets ResourceName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


