# \VolumesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VolumesControllerCreate**](VolumesApi.md#VolumesControllerCreate) | **Post** /v0/states/{state}/volumes | Create a new Volume
[**VolumesControllerDelete**](VolumesApi.md#VolumesControllerDelete) | **Delete** /v0/states/{state}/volumes/{volume} | Delete a Volume
[**VolumesControllerFindMany**](VolumesApi.md#VolumesControllerFindMany) | **Get** /v0/states/{state}/volumes | Get all Volumes
[**VolumesControllerFindMany_0**](VolumesApi.md#VolumesControllerFindMany_0) | **Get** /v0/states/{state}/volumes | Get all Volumes
[**VolumesControllerFindOne**](VolumesApi.md#VolumesControllerFindOne) | **Get** /v0/states/{state}/volumes/{volume} | Return specific Volume
[**VolumesControllerUpdateFormatted**](VolumesApi.md#VolumesControllerUpdateFormatted) | **Put** /v0/states/{state}/volumes/{volume}/formattedDate | Update format date of a Volume



## VolumesControllerCreate

> Volume VolumesControllerCreate(ctx, state).CreateVolumeDto(createVolumeDto).Execute()

Create a new Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    createVolumeDto := *openapiclient.NewCreateVolumeDto("Name_example", int32(123), "FsType_example") // CreateVolumeDto | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerCreate(context.Background(), state).CreateVolumeDto(createVolumeDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerCreate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerCreate`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerCreate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerCreateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createVolumeDto** | [**CreateVolumeDto**](CreateVolumeDto.md) |  | 

### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumesControllerDelete

> Volume VolumesControllerDelete(ctx, state, volume).Execute()

Delete a Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    volume := "volume_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerDelete(context.Background(), state, volume).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerDelete`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerDelete`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**volume** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumesControllerFindMany

> []Volume VolumesControllerFindMany(ctx, state).Execute()

Get all Volumes

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerFindMany(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerFindMany``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerFindMany`: []Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerFindMany`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerFindManyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumesControllerFindMany_0

> []Volume VolumesControllerFindMany_0(ctx, state).Execute()

Get all Volumes

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerFindMany_0(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerFindMany_0``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerFindMany_0`: []Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerFindMany_0`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerFindMany_1Request struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumesControllerFindOne

> Volume VolumesControllerFindOne(ctx, state, volume).Execute()

Return specific Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    volume := "volume_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerFindOne(context.Background(), state, volume).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerFindOne``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerFindOne`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerFindOne`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**volume** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerFindOneRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumesControllerUpdateFormatted

> Volume VolumesControllerUpdateFormatted(ctx, state, volume).Execute()

Update format date of a Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    volume := "volume_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumesApi.VolumesControllerUpdateFormatted(context.Background(), state, volume).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumesApi.VolumesControllerUpdateFormatted``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumesControllerUpdateFormatted`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumesApi.VolumesControllerUpdateFormatted`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**volume** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumesControllerUpdateFormattedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

