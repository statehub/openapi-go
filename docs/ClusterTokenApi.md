# \ClusterTokenApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ClusterTokenControllerCheck**](ClusterTokenApi.md#ClusterTokenControllerCheck) | **Get** /v0/clusters/{cluster}/token | Check if there is a Token generated for the Cluster
[**ClusterTokenControllerDelete**](ClusterTokenApi.md#ClusterTokenControllerDelete) | **Delete** /v0/clusters/{cluster}/token | Revoke a Cluster&#39;s Token
[**ClusterTokenControllerGenerate**](ClusterTokenApi.md#ClusterTokenControllerGenerate) | **Post** /v0/clusters/{cluster}/token | Generate a Token



## ClusterTokenControllerCheck

> ClusterTokenPlaceholder ClusterTokenControllerCheck(ctx, cluster).Execute()

Check if there is a Token generated for the Cluster

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterTokenApi.ClusterTokenControllerCheck(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterTokenApi.ClusterTokenControllerCheck``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterTokenControllerCheck`: ClusterTokenPlaceholder
    fmt.Fprintf(os.Stdout, "Response from `ClusterTokenApi.ClusterTokenControllerCheck`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterTokenControllerCheckRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ClusterTokenPlaceholder**](ClusterTokenPlaceholder.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ClusterTokenControllerDelete

> ClusterTokenControllerDelete(ctx, cluster).Execute()

Revoke a Cluster's Token

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterTokenApi.ClusterTokenControllerDelete(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterTokenApi.ClusterTokenControllerDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterTokenControllerDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ClusterTokenControllerGenerate

> ClusterToken ClusterTokenControllerGenerate(ctx, cluster).Execute()

Generate a Token

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterTokenApi.ClusterTokenControllerGenerate(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterTokenApi.ClusterTokenControllerGenerate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterTokenControllerGenerate`: ClusterToken
    fmt.Fprintf(os.Stdout, "Response from `ClusterTokenApi.ClusterTokenControllerGenerate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterTokenControllerGenerateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ClusterToken**](ClusterToken.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

