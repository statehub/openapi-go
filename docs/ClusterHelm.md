# ClusterHelm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Repo** | **string** | Helm  description | 
**Chart** | **string** | Helm chart | 
**Version** | **string** | Helm version | 
**Parameters** | **map[string]interface{}** | Helm parameters | 

## Methods

### NewClusterHelm

`func NewClusterHelm(repo string, chart string, version string, parameters map[string]interface{}, ) *ClusterHelm`

NewClusterHelm instantiates a new ClusterHelm object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterHelmWithDefaults

`func NewClusterHelmWithDefaults() *ClusterHelm`

NewClusterHelmWithDefaults instantiates a new ClusterHelm object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRepo

`func (o *ClusterHelm) GetRepo() string`

GetRepo returns the Repo field if non-nil, zero value otherwise.

### GetRepoOk

`func (o *ClusterHelm) GetRepoOk() (*string, bool)`

GetRepoOk returns a tuple with the Repo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRepo

`func (o *ClusterHelm) SetRepo(v string)`

SetRepo sets Repo field to given value.


### GetChart

`func (o *ClusterHelm) GetChart() string`

GetChart returns the Chart field if non-nil, zero value otherwise.

### GetChartOk

`func (o *ClusterHelm) GetChartOk() (*string, bool)`

GetChartOk returns a tuple with the Chart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChart

`func (o *ClusterHelm) SetChart(v string)`

SetChart sets Chart field to given value.


### GetVersion

`func (o *ClusterHelm) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *ClusterHelm) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *ClusterHelm) SetVersion(v string)`

SetVersion sets Version field to given value.


### GetParameters

`func (o *ClusterHelm) GetParameters() map[string]interface{}`

GetParameters returns the Parameters field if non-nil, zero value otherwise.

### GetParametersOk

`func (o *ClusterHelm) GetParametersOk() (*map[string]interface{}, bool)`

GetParametersOk returns a tuple with the Parameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParameters

`func (o *ClusterHelm) SetParameters(v map[string]interface{})`

SetParameters sets Parameters field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


