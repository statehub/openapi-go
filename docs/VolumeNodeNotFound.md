# VolumeNodeNotFound

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Volume** | **string** |  | 
**Node** | **string** |  | 
**Cluster** | **string** |  | 

## Methods

### NewVolumeNodeNotFound

`func NewVolumeNodeNotFound(errorCode string, volume string, node string, cluster string, ) *VolumeNodeNotFound`

NewVolumeNodeNotFound instantiates a new VolumeNodeNotFound object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeNodeNotFoundWithDefaults

`func NewVolumeNodeNotFoundWithDefaults() *VolumeNodeNotFound`

NewVolumeNodeNotFoundWithDefaults instantiates a new VolumeNodeNotFound object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *VolumeNodeNotFound) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *VolumeNodeNotFound) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *VolumeNodeNotFound) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetVolume

`func (o *VolumeNodeNotFound) GetVolume() string`

GetVolume returns the Volume field if non-nil, zero value otherwise.

### GetVolumeOk

`func (o *VolumeNodeNotFound) GetVolumeOk() (*string, bool)`

GetVolumeOk returns a tuple with the Volume field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolume

`func (o *VolumeNodeNotFound) SetVolume(v string)`

SetVolume sets Volume field to given value.


### GetNode

`func (o *VolumeNodeNotFound) GetNode() string`

GetNode returns the Node field if non-nil, zero value otherwise.

### GetNodeOk

`func (o *VolumeNodeNotFound) GetNodeOk() (*string, bool)`

GetNodeOk returns a tuple with the Node field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNode

`func (o *VolumeNodeNotFound) SetNode(v string)`

SetNode sets Node field to given value.


### GetCluster

`func (o *VolumeNodeNotFound) GetCluster() string`

GetCluster returns the Cluster field if non-nil, zero value otherwise.

### GetClusterOk

`func (o *VolumeNodeNotFound) GetClusterOk() (*string, bool)`

GetClusterOk returns a tuple with the Cluster field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCluster

`func (o *VolumeNodeNotFound) SetCluster(v string)`

SetCluster sets Cluster field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


