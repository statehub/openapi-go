# CloudNotSupported

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Cloud** | **string** |  | 

## Methods

### NewCloudNotSupported

`func NewCloudNotSupported(errorCode string, cloud string, ) *CloudNotSupported`

NewCloudNotSupported instantiates a new CloudNotSupported object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloudNotSupportedWithDefaults

`func NewCloudNotSupportedWithDefaults() *CloudNotSupported`

NewCloudNotSupportedWithDefaults instantiates a new CloudNotSupported object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *CloudNotSupported) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *CloudNotSupported) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *CloudNotSupported) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetCloud

`func (o *CloudNotSupported) GetCloud() string`

GetCloud returns the Cloud field if non-nil, zero value otherwise.

### GetCloudOk

`func (o *CloudNotSupported) GetCloudOk() (*string, bool)`

GetCloudOk returns a tuple with the Cloud field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCloud

`func (o *CloudNotSupported) SetCloud(v string)`

SetCloud sets Cloud field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


