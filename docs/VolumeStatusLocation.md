# VolumeStatusLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Location** | **string** | The active location for the Volume. Format is &#x60;{cloud}/{region}&#x60;. Should match one of the locations of the State | 
**Progress** | [**VolumeStatusLocationProgress**](VolumeStatusLocationProgress.md) | Progress (if applicable) | 
**Status** | **string** | State status | 

## Methods

### NewVolumeStatusLocation

`func NewVolumeStatusLocation(location string, progress VolumeStatusLocationProgress, status string, ) *VolumeStatusLocation`

NewVolumeStatusLocation instantiates a new VolumeStatusLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusLocationWithDefaults

`func NewVolumeStatusLocationWithDefaults() *VolumeStatusLocation`

NewVolumeStatusLocationWithDefaults instantiates a new VolumeStatusLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocation

`func (o *VolumeStatusLocation) GetLocation() string`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *VolumeStatusLocation) GetLocationOk() (*string, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *VolumeStatusLocation) SetLocation(v string)`

SetLocation sets Location field to given value.


### GetProgress

`func (o *VolumeStatusLocation) GetProgress() VolumeStatusLocationProgress`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *VolumeStatusLocation) GetProgressOk() (*VolumeStatusLocationProgress, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *VolumeStatusLocation) SetProgress(v VolumeStatusLocationProgress)`

SetProgress sets Progress field to given value.


### GetStatus

`func (o *VolumeStatusLocation) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *VolumeStatusLocation) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *VolumeStatusLocation) SetStatus(v string)`

SetStatus sets Status field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


