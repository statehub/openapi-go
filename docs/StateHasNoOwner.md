# StateHasNoOwner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**State** | **string** |  | 

## Methods

### NewStateHasNoOwner

`func NewStateHasNoOwner(errorCode string, state string, ) *StateHasNoOwner`

NewStateHasNoOwner instantiates a new StateHasNoOwner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateHasNoOwnerWithDefaults

`func NewStateHasNoOwnerWithDefaults() *StateHasNoOwner`

NewStateHasNoOwnerWithDefaults instantiates a new StateHasNoOwner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *StateHasNoOwner) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *StateHasNoOwner) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *StateHasNoOwner) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetState

`func (o *StateHasNoOwner) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *StateHasNoOwner) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *StateHasNoOwner) SetState(v string)`

SetState sets State field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


