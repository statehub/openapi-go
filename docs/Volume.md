# Volume

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The unique ID of object | 
**Name** | **string** | Volume name | 
**SizeGi** | **int32** | The size of the Volume in GiB | 
**FsType** | **string** | The file system type of the Volume | 
**ActiveLocation** | Pointer to **string** | The active location for the Volume. Format is &#x60;{cloud}/{region}&#x60;. Should match one of the locations of the State | [optional] 
**Locations** | [**[]VolumeLocation**](VolumeLocation.md) | Volume status in this location | 
**Format** | Pointer to **time.Time** | Format date | [optional] 
**Created** | **time.Time** | Creation date | 
**Modified** | **time.Time** | Modify date | 

## Methods

### NewVolume

`func NewVolume(id string, name string, sizeGi int32, fsType string, locations []VolumeLocation, created time.Time, modified time.Time, ) *Volume`

NewVolume instantiates a new Volume object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeWithDefaults

`func NewVolumeWithDefaults() *Volume`

NewVolumeWithDefaults instantiates a new Volume object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Volume) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Volume) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Volume) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *Volume) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Volume) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Volume) SetName(v string)`

SetName sets Name field to given value.


### GetSizeGi

`func (o *Volume) GetSizeGi() int32`

GetSizeGi returns the SizeGi field if non-nil, zero value otherwise.

### GetSizeGiOk

`func (o *Volume) GetSizeGiOk() (*int32, bool)`

GetSizeGiOk returns a tuple with the SizeGi field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSizeGi

`func (o *Volume) SetSizeGi(v int32)`

SetSizeGi sets SizeGi field to given value.


### GetFsType

`func (o *Volume) GetFsType() string`

GetFsType returns the FsType field if non-nil, zero value otherwise.

### GetFsTypeOk

`func (o *Volume) GetFsTypeOk() (*string, bool)`

GetFsTypeOk returns a tuple with the FsType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFsType

`func (o *Volume) SetFsType(v string)`

SetFsType sets FsType field to given value.


### GetActiveLocation

`func (o *Volume) GetActiveLocation() string`

GetActiveLocation returns the ActiveLocation field if non-nil, zero value otherwise.

### GetActiveLocationOk

`func (o *Volume) GetActiveLocationOk() (*string, bool)`

GetActiveLocationOk returns a tuple with the ActiveLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActiveLocation

`func (o *Volume) SetActiveLocation(v string)`

SetActiveLocation sets ActiveLocation field to given value.

### HasActiveLocation

`func (o *Volume) HasActiveLocation() bool`

HasActiveLocation returns a boolean if a field has been set.

### GetLocations

`func (o *Volume) GetLocations() []VolumeLocation`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *Volume) GetLocationsOk() (*[]VolumeLocation, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *Volume) SetLocations(v []VolumeLocation)`

SetLocations sets Locations field to given value.


### GetFormat

`func (o *Volume) GetFormat() time.Time`

GetFormat returns the Format field if non-nil, zero value otherwise.

### GetFormatOk

`func (o *Volume) GetFormatOk() (*time.Time, bool)`

GetFormatOk returns a tuple with the Format field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormat

`func (o *Volume) SetFormat(v time.Time)`

SetFormat sets Format field to given value.

### HasFormat

`func (o *Volume) HasFormat() bool`

HasFormat returns a boolean if a field has been set.

### GetCreated

`func (o *Volume) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *Volume) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *Volume) SetCreated(v time.Time)`

SetCreated sets Created field to given value.


### GetModified

`func (o *Volume) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *Volume) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *Volume) SetModified(v time.Time)`

SetModified sets Modified field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


