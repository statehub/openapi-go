# StateLocationAzurePls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Private link service id | 

## Methods

### NewStateLocationAzurePls

`func NewStateLocationAzurePls(id string, ) *StateLocationAzurePls`

NewStateLocationAzurePls instantiates a new StateLocationAzurePls object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationAzurePlsWithDefaults

`func NewStateLocationAzurePlsWithDefaults() *StateLocationAzurePls`

NewStateLocationAzurePlsWithDefaults instantiates a new StateLocationAzurePls object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *StateLocationAzurePls) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *StateLocationAzurePls) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *StateLocationAzurePls) SetId(v string)`

SetId sets Id field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


