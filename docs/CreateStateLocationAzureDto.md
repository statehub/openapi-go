# CreateStateLocationAzureDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Region** | **string** | An Azure Region | 

## Methods

### NewCreateStateLocationAzureDto

`func NewCreateStateLocationAzureDto(region string, ) *CreateStateLocationAzureDto`

NewCreateStateLocationAzureDto instantiates a new CreateStateLocationAzureDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStateLocationAzureDtoWithDefaults

`func NewCreateStateLocationAzureDtoWithDefaults() *CreateStateLocationAzureDto`

NewCreateStateLocationAzureDtoWithDefaults instantiates a new CreateStateLocationAzureDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRegion

`func (o *CreateStateLocationAzureDto) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *CreateStateLocationAzureDto) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *CreateStateLocationAzureDto) SetRegion(v string)`

SetRegion sets Region field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


