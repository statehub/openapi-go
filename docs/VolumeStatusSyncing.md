# VolumeStatusSyncing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Syncing** | **[]string** | Synchronization progress by location | 

## Methods

### NewVolumeStatusSyncing

`func NewVolumeStatusSyncing(syncing []string, ) *VolumeStatusSyncing`

NewVolumeStatusSyncing instantiates a new VolumeStatusSyncing object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusSyncingWithDefaults

`func NewVolumeStatusSyncingWithDefaults() *VolumeStatusSyncing`

NewVolumeStatusSyncingWithDefaults instantiates a new VolumeStatusSyncing object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSyncing

`func (o *VolumeStatusSyncing) GetSyncing() []string`

GetSyncing returns the Syncing field if non-nil, zero value otherwise.

### GetSyncingOk

`func (o *VolumeStatusSyncing) GetSyncingOk() (*[]string, bool)`

GetSyncingOk returns a tuple with the Syncing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSyncing

`func (o *VolumeStatusSyncing) SetSyncing(v []string)`

SetSyncing sets Syncing field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


