# StateNameInvalid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Name** | **string** |  | 

## Methods

### NewStateNameInvalid

`func NewStateNameInvalid(errorCode string, name string, ) *StateNameInvalid`

NewStateNameInvalid instantiates a new StateNameInvalid object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateNameInvalidWithDefaults

`func NewStateNameInvalidWithDefaults() *StateNameInvalid`

NewStateNameInvalidWithDefaults instantiates a new StateNameInvalid object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *StateNameInvalid) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *StateNameInvalid) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *StateNameInvalid) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetName

`func (o *StateNameInvalid) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *StateNameInvalid) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *StateNameInvalid) SetName(v string)`

SetName sets Name field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


