# CreateStateLocations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | Pointer to **[]string** | The AWS locations for the State | [optional] 
**Azure** | Pointer to **[]string** | The Azure locations for the State | [optional] 

## Methods

### NewCreateStateLocations

`func NewCreateStateLocations() *CreateStateLocations`

NewCreateStateLocations instantiates a new CreateStateLocations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStateLocationsWithDefaults

`func NewCreateStateLocationsWithDefaults() *CreateStateLocations`

NewCreateStateLocationsWithDefaults instantiates a new CreateStateLocations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *CreateStateLocations) GetAws() []string`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *CreateStateLocations) GetAwsOk() (*[]string, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *CreateStateLocations) SetAws(v []string)`

SetAws sets Aws field to given value.

### HasAws

`func (o *CreateStateLocations) HasAws() bool`

HasAws returns a boolean if a field has been set.

### GetAzure

`func (o *CreateStateLocations) GetAzure() []string`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *CreateStateLocations) GetAzureOk() (*[]string, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *CreateStateLocations) SetAzure(v []string)`

SetAzure sets Azure field to given value.

### HasAzure

`func (o *CreateStateLocations) HasAzure() bool`

HasAzure returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


