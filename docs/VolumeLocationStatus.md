# VolumeLocationStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **string** | Volume location status value | 
**Msg** | Pointer to **string** | A human readable message explaining the status | [optional] 

## Methods

### NewVolumeLocationStatus

`func NewVolumeLocationStatus(value string, ) *VolumeLocationStatus`

NewVolumeLocationStatus instantiates a new VolumeLocationStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeLocationStatusWithDefaults

`func NewVolumeLocationStatusWithDefaults() *VolumeLocationStatus`

NewVolumeLocationStatusWithDefaults instantiates a new VolumeLocationStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *VolumeLocationStatus) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *VolumeLocationStatus) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *VolumeLocationStatus) SetValue(v string)`

SetValue sets Value field to given value.


### GetMsg

`func (o *VolumeLocationStatus) GetMsg() string`

GetMsg returns the Msg field if non-nil, zero value otherwise.

### GetMsgOk

`func (o *VolumeLocationStatus) GetMsgOk() (*string, bool)`

GetMsgOk returns a tuple with the Msg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMsg

`func (o *VolumeLocationStatus) SetMsg(v string)`

SetMsg sets Msg field to given value.

### HasMsg

`func (o *VolumeLocationStatus) HasMsg() bool`

HasMsg returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


