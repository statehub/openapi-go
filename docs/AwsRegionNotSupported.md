# AwsRegionNotSupported

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Region** | **string** |  | 

## Methods

### NewAwsRegionNotSupported

`func NewAwsRegionNotSupported(errorCode string, region string, ) *AwsRegionNotSupported`

NewAwsRegionNotSupported instantiates a new AwsRegionNotSupported object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAwsRegionNotSupportedWithDefaults

`func NewAwsRegionNotSupportedWithDefaults() *AwsRegionNotSupported`

NewAwsRegionNotSupportedWithDefaults instantiates a new AwsRegionNotSupported object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *AwsRegionNotSupported) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *AwsRegionNotSupported) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *AwsRegionNotSupported) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetRegion

`func (o *AwsRegionNotSupported) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *AwsRegionNotSupported) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *AwsRegionNotSupported) SetRegion(v string)`

SetRegion sets Region field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


