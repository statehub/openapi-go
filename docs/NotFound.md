# NotFound

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HttpCode** | **float32** | The HTTP Code | 
**HttpStatus** | **string** | The HTTP Status | 

## Methods

### NewNotFound

`func NewNotFound(httpCode float32, httpStatus string, ) *NotFound`

NewNotFound instantiates a new NotFound object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotFoundWithDefaults

`func NewNotFoundWithDefaults() *NotFound`

NewNotFoundWithDefaults instantiates a new NotFound object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHttpCode

`func (o *NotFound) GetHttpCode() float32`

GetHttpCode returns the HttpCode field if non-nil, zero value otherwise.

### GetHttpCodeOk

`func (o *NotFound) GetHttpCodeOk() (*float32, bool)`

GetHttpCodeOk returns a tuple with the HttpCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHttpCode

`func (o *NotFound) SetHttpCode(v float32)`

SetHttpCode sets HttpCode field to given value.


### GetHttpStatus

`func (o *NotFound) GetHttpStatus() string`

GetHttpStatus returns the HttpStatus field if non-nil, zero value otherwise.

### GetHttpStatusOk

`func (o *NotFound) GetHttpStatusOk() (*string, bool)`

GetHttpStatusOk returns a tuple with the HttpStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHttpStatus

`func (o *NotFound) SetHttpStatus(v string)`

SetHttpStatus sets HttpStatus field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


