# CreateStateLocationAws

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Region** | **string** | An AWS Region | 

## Methods

### NewCreateStateLocationAws

`func NewCreateStateLocationAws(region string, ) *CreateStateLocationAws`

NewCreateStateLocationAws instantiates a new CreateStateLocationAws object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStateLocationAwsWithDefaults

`func NewCreateStateLocationAwsWithDefaults() *CreateStateLocationAws`

NewCreateStateLocationAwsWithDefaults instantiates a new CreateStateLocationAws object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRegion

`func (o *CreateStateLocationAws) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *CreateStateLocationAws) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *CreateStateLocationAws) SetRegion(v string)`

SetRegion sets Region field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


