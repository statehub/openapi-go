# VolumeLocationIscsi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Iqn** | **string** | Volume location iSCSI iqn | 
**Ipv4** | Pointer to **string** | Volume location iSCSI ipv4 | [optional] 
**Port** | Pointer to **float32** | Volume location iSCSI port | [optional] 
**Chap** | Pointer to [**VolumeLocationIscsiChap**](VolumeLocationIscsiChap.md) | Volume location iSCSI chap | [optional] 

## Methods

### NewVolumeLocationIscsi

`func NewVolumeLocationIscsi(iqn string, ) *VolumeLocationIscsi`

NewVolumeLocationIscsi instantiates a new VolumeLocationIscsi object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeLocationIscsiWithDefaults

`func NewVolumeLocationIscsiWithDefaults() *VolumeLocationIscsi`

NewVolumeLocationIscsiWithDefaults instantiates a new VolumeLocationIscsi object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIqn

`func (o *VolumeLocationIscsi) GetIqn() string`

GetIqn returns the Iqn field if non-nil, zero value otherwise.

### GetIqnOk

`func (o *VolumeLocationIscsi) GetIqnOk() (*string, bool)`

GetIqnOk returns a tuple with the Iqn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIqn

`func (o *VolumeLocationIscsi) SetIqn(v string)`

SetIqn sets Iqn field to given value.


### GetIpv4

`func (o *VolumeLocationIscsi) GetIpv4() string`

GetIpv4 returns the Ipv4 field if non-nil, zero value otherwise.

### GetIpv4Ok

`func (o *VolumeLocationIscsi) GetIpv4Ok() (*string, bool)`

GetIpv4Ok returns a tuple with the Ipv4 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpv4

`func (o *VolumeLocationIscsi) SetIpv4(v string)`

SetIpv4 sets Ipv4 field to given value.

### HasIpv4

`func (o *VolumeLocationIscsi) HasIpv4() bool`

HasIpv4 returns a boolean if a field has been set.

### GetPort

`func (o *VolumeLocationIscsi) GetPort() float32`

GetPort returns the Port field if non-nil, zero value otherwise.

### GetPortOk

`func (o *VolumeLocationIscsi) GetPortOk() (*float32, bool)`

GetPortOk returns a tuple with the Port field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPort

`func (o *VolumeLocationIscsi) SetPort(v float32)`

SetPort sets Port field to given value.

### HasPort

`func (o *VolumeLocationIscsi) HasPort() bool`

HasPort returns a boolean if a field has been set.

### GetChap

`func (o *VolumeLocationIscsi) GetChap() VolumeLocationIscsiChap`

GetChap returns the Chap field if non-nil, zero value otherwise.

### GetChapOk

`func (o *VolumeLocationIscsi) GetChapOk() (*VolumeLocationIscsiChap, bool)`

GetChapOk returns a tuple with the Chap field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChap

`func (o *VolumeLocationIscsi) SetChap(v VolumeLocationIscsiChap)`

SetChap sets Chap field to given value.

### HasChap

`func (o *VolumeLocationIscsi) HasChap() bool`

HasChap returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


