# StateLocationStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **string** | State status | 
**Msg** | **string** | A human readable message explaining the status | 

## Methods

### NewStateLocationStatus

`func NewStateLocationStatus(value string, msg string, ) *StateLocationStatus`

NewStateLocationStatus instantiates a new StateLocationStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationStatusWithDefaults

`func NewStateLocationStatusWithDefaults() *StateLocationStatus`

NewStateLocationStatusWithDefaults instantiates a new StateLocationStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *StateLocationStatus) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *StateLocationStatus) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *StateLocationStatus) SetValue(v string)`

SetValue sets Value field to given value.


### GetMsg

`func (o *StateLocationStatus) GetMsg() string`

GetMsg returns the Msg field if non-nil, zero value otherwise.

### GetMsgOk

`func (o *StateLocationStatus) GetMsgOk() (*string, bool)`

GetMsgOk returns a tuple with the Msg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMsg

`func (o *StateLocationStatus) SetMsg(v string)`

SetMsg sets Msg field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


