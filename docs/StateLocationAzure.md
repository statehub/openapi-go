# StateLocationAzure

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Region** | **string** | An Azure Region | 
**Status** | **string** | Location status | 
**Volumes** | [**[]StateLocationVolume**](StateLocationVolume.md) | Volume status in this location | 
**PrivateLinkService** | Pointer to [**StateLocationAzurePls**](StateLocationAzurePls.md) | PrivateLink Endpoint | [optional] 

## Methods

### NewStateLocationAzure

`func NewStateLocationAzure(region string, status string, volumes []StateLocationVolume, ) *StateLocationAzure`

NewStateLocationAzure instantiates a new StateLocationAzure object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationAzureWithDefaults

`func NewStateLocationAzureWithDefaults() *StateLocationAzure`

NewStateLocationAzureWithDefaults instantiates a new StateLocationAzure object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRegion

`func (o *StateLocationAzure) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *StateLocationAzure) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *StateLocationAzure) SetRegion(v string)`

SetRegion sets Region field to given value.


### GetStatus

`func (o *StateLocationAzure) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *StateLocationAzure) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *StateLocationAzure) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetVolumes

`func (o *StateLocationAzure) GetVolumes() []StateLocationVolume`

GetVolumes returns the Volumes field if non-nil, zero value otherwise.

### GetVolumesOk

`func (o *StateLocationAzure) GetVolumesOk() (*[]StateLocationVolume, bool)`

GetVolumesOk returns a tuple with the Volumes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolumes

`func (o *StateLocationAzure) SetVolumes(v []StateLocationVolume)`

SetVolumes sets Volumes field to given value.


### GetPrivateLinkService

`func (o *StateLocationAzure) GetPrivateLinkService() StateLocationAzurePls`

GetPrivateLinkService returns the PrivateLinkService field if non-nil, zero value otherwise.

### GetPrivateLinkServiceOk

`func (o *StateLocationAzure) GetPrivateLinkServiceOk() (*StateLocationAzurePls, bool)`

GetPrivateLinkServiceOk returns a tuple with the PrivateLinkService field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrivateLinkService

`func (o *StateLocationAzure) SetPrivateLinkService(v StateLocationAzurePls)`

SetPrivateLinkService sets PrivateLinkService field to given value.

### HasPrivateLinkService

`func (o *StateLocationAzure) HasPrivateLinkService() bool`

HasPrivateLinkService returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


