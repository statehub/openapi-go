# VolumeStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **string** | Volume status | 
**Msg** | Pointer to **string** | A human readable message explaining the status | [optional] 

## Methods

### NewVolumeStatus

`func NewVolumeStatus(value string, ) *VolumeStatus`

NewVolumeStatus instantiates a new VolumeStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusWithDefaults

`func NewVolumeStatusWithDefaults() *VolumeStatus`

NewVolumeStatusWithDefaults instantiates a new VolumeStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *VolumeStatus) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *VolumeStatus) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *VolumeStatus) SetValue(v string)`

SetValue sets Value field to given value.


### GetMsg

`func (o *VolumeStatus) GetMsg() string`

GetMsg returns the Msg field if non-nil, zero value otherwise.

### GetMsgOk

`func (o *VolumeStatus) GetMsgOk() (*string, bool)`

GetMsgOk returns a tuple with the Msg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMsg

`func (o *VolumeStatus) SetMsg(v string)`

SetMsg sets Msg field to given value.

### HasMsg

`func (o *VolumeStatus) HasMsg() bool`

HasMsg returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


