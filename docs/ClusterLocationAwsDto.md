# ClusterLocationAwsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Region** | **string** | An AWS Region | 
**AccountPrincipal** | **string** | AWS Account Principal | 

## Methods

### NewClusterLocationAwsDto

`func NewClusterLocationAwsDto(region string, accountPrincipal string, ) *ClusterLocationAwsDto`

NewClusterLocationAwsDto instantiates a new ClusterLocationAwsDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterLocationAwsDtoWithDefaults

`func NewClusterLocationAwsDtoWithDefaults() *ClusterLocationAwsDto`

NewClusterLocationAwsDtoWithDefaults instantiates a new ClusterLocationAwsDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRegion

`func (o *ClusterLocationAwsDto) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *ClusterLocationAwsDto) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *ClusterLocationAwsDto) SetRegion(v string)`

SetRegion sets Region field to given value.


### GetAccountPrincipal

`func (o *ClusterLocationAwsDto) GetAccountPrincipal() string`

GetAccountPrincipal returns the AccountPrincipal field if non-nil, zero value otherwise.

### GetAccountPrincipalOk

`func (o *ClusterLocationAwsDto) GetAccountPrincipalOk() (*string, bool)`

GetAccountPrincipalOk returns a tuple with the AccountPrincipal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccountPrincipal

`func (o *ClusterLocationAwsDto) SetAccountPrincipal(v string)`

SetAccountPrincipal sets AccountPrincipal field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


