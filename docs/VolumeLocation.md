# VolumeLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | [**VolumeLocationStatus**](VolumeLocationStatus.md) | Volume location status | 
**Progress** | Pointer to [**VolumeLocationProgress**](VolumeLocationProgress.md) | Progress (if applicable) | [optional] 
**Name** | **string** | The active location for the Volume. Format is &#x60;{cloud}/{region}&#x60;. Should match one of the locations of the State | 
**Iscsi** | Pointer to [**VolumeLocationIscsi**](VolumeLocationIscsi.md) | Volume location iSCSI access object | [optional] 

## Methods

### NewVolumeLocation

`func NewVolumeLocation(status VolumeLocationStatus, name string, ) *VolumeLocation`

NewVolumeLocation instantiates a new VolumeLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeLocationWithDefaults

`func NewVolumeLocationWithDefaults() *VolumeLocation`

NewVolumeLocationWithDefaults instantiates a new VolumeLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *VolumeLocation) GetStatus() VolumeLocationStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *VolumeLocation) GetStatusOk() (*VolumeLocationStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *VolumeLocation) SetStatus(v VolumeLocationStatus)`

SetStatus sets Status field to given value.


### GetProgress

`func (o *VolumeLocation) GetProgress() VolumeLocationProgress`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *VolumeLocation) GetProgressOk() (*VolumeLocationProgress, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *VolumeLocation) SetProgress(v VolumeLocationProgress)`

SetProgress sets Progress field to given value.

### HasProgress

`func (o *VolumeLocation) HasProgress() bool`

HasProgress returns a boolean if a field has been set.

### GetName

`func (o *VolumeLocation) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *VolumeLocation) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *VolumeLocation) SetName(v string)`

SetName sets Name field to given value.


### GetIscsi

`func (o *VolumeLocation) GetIscsi() VolumeLocationIscsi`

GetIscsi returns the Iscsi field if non-nil, zero value otherwise.

### GetIscsiOk

`func (o *VolumeLocation) GetIscsiOk() (*VolumeLocationIscsi, bool)`

GetIscsiOk returns a tuple with the Iscsi field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIscsi

`func (o *VolumeLocation) SetIscsi(v VolumeLocationIscsi)`

SetIscsi sets Iscsi field to given value.

### HasIscsi

`func (o *VolumeLocation) HasIscsi() bool`

HasIscsi returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


