# VolumeStatusPending

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Pending** | **map[string]interface{}** | Volume is pending. Should be &#x60;ok&#x60; soon. | 

## Methods

### NewVolumeStatusPending

`func NewVolumeStatusPending(pending map[string]interface{}, ) *VolumeStatusPending`

NewVolumeStatusPending instantiates a new VolumeStatusPending object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusPendingWithDefaults

`func NewVolumeStatusPendingWithDefaults() *VolumeStatusPending`

NewVolumeStatusPendingWithDefaults instantiates a new VolumeStatusPending object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPending

`func (o *VolumeStatusPending) GetPending() map[string]interface{}`

GetPending returns the Pending field if non-nil, zero value otherwise.

### GetPendingOk

`func (o *VolumeStatusPending) GetPendingOk() (*map[string]interface{}, bool)`

GetPendingOk returns a tuple with the Pending field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPending

`func (o *VolumeStatusPending) SetPending(v map[string]interface{})`

SetPending sets Pending field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


