# StateLocationStatusOk

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ok** | **map[string]interface{}** | Location is ok. | 

## Methods

### NewStateLocationStatusOk

`func NewStateLocationStatusOk(ok map[string]interface{}, ) *StateLocationStatusOk`

NewStateLocationStatusOk instantiates a new StateLocationStatusOk object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationStatusOkWithDefaults

`func NewStateLocationStatusOkWithDefaults() *StateLocationStatusOk`

NewStateLocationStatusOkWithDefaults instantiates a new StateLocationStatusOk object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOk

`func (o *StateLocationStatusOk) GetOk() map[string]interface{}`

GetOk returns the Ok field if non-nil, zero value otherwise.

### GetOkOk

`func (o *StateLocationStatusOk) GetOkOk() (*map[string]interface{}, bool)`

GetOkOk returns a tuple with the Ok field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOk

`func (o *StateLocationStatusOk) SetOk(v map[string]interface{})`

SetOk sets Ok field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


