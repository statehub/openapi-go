# ClusterTokenAlreadyExists

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Cluster** | **string** |  | 

## Methods

### NewClusterTokenAlreadyExists

`func NewClusterTokenAlreadyExists(errorCode string, cluster string, ) *ClusterTokenAlreadyExists`

NewClusterTokenAlreadyExists instantiates a new ClusterTokenAlreadyExists object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterTokenAlreadyExistsWithDefaults

`func NewClusterTokenAlreadyExistsWithDefaults() *ClusterTokenAlreadyExists`

NewClusterTokenAlreadyExistsWithDefaults instantiates a new ClusterTokenAlreadyExists object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *ClusterTokenAlreadyExists) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *ClusterTokenAlreadyExists) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *ClusterTokenAlreadyExists) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetCluster

`func (o *ClusterTokenAlreadyExists) GetCluster() string`

GetCluster returns the Cluster field if non-nil, zero value otherwise.

### GetClusterOk

`func (o *ClusterTokenAlreadyExists) GetClusterOk() (*string, bool)`

GetClusterOk returns a tuple with the Cluster field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCluster

`func (o *ClusterTokenAlreadyExists) SetCluster(v string)`

SetCluster sets Cluster field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


