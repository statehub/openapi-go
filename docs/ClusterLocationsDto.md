# ClusterLocationsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | [**[]ClusterLocationAwsDto**](ClusterLocationAwsDto.md) | The AWS locations for the Cluster | 
**Azure** | [**[]ClusterLocationAzureDto**](ClusterLocationAzureDto.md) | The Azure locations for the Cluster | 

## Methods

### NewClusterLocationsDto

`func NewClusterLocationsDto(aws []ClusterLocationAwsDto, azure []ClusterLocationAzureDto, ) *ClusterLocationsDto`

NewClusterLocationsDto instantiates a new ClusterLocationsDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterLocationsDtoWithDefaults

`func NewClusterLocationsDtoWithDefaults() *ClusterLocationsDto`

NewClusterLocationsDtoWithDefaults instantiates a new ClusterLocationsDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *ClusterLocationsDto) GetAws() []ClusterLocationAwsDto`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *ClusterLocationsDto) GetAwsOk() (*[]ClusterLocationAwsDto, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *ClusterLocationsDto) SetAws(v []ClusterLocationAwsDto)`

SetAws sets Aws field to given value.


### GetAzure

`func (o *ClusterLocationsDto) GetAzure() []ClusterLocationAzureDto`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *ClusterLocationsDto) GetAzureOk() (*[]ClusterLocationAzureDto, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *ClusterLocationsDto) SetAzure(v []ClusterLocationAzureDto)`

SetAzure sets Azure field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


