# ClusterTokenPlaceholder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Token** | **string** | A placeholder for the Cluster&#39;s Token (&#39;xxx&#39;) | 

## Methods

### NewClusterTokenPlaceholder

`func NewClusterTokenPlaceholder(token string, ) *ClusterTokenPlaceholder`

NewClusterTokenPlaceholder instantiates a new ClusterTokenPlaceholder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterTokenPlaceholderWithDefaults

`func NewClusterTokenPlaceholderWithDefaults() *ClusterTokenPlaceholder`

NewClusterTokenPlaceholderWithDefaults instantiates a new ClusterTokenPlaceholder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetToken

`func (o *ClusterTokenPlaceholder) GetToken() string`

GetToken returns the Token field if non-nil, zero value otherwise.

### GetTokenOk

`func (o *ClusterTokenPlaceholder) GetTokenOk() (*string, bool)`

GetTokenOk returns a tuple with the Token field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetToken

`func (o *ClusterTokenPlaceholder) SetToken(v string)`

SetToken sets Token field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


