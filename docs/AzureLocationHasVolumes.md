# AzureLocationHasVolumes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**State** | **string** |  | 
**Region** | **string** |  | 

## Methods

### NewAzureLocationHasVolumes

`func NewAzureLocationHasVolumes(errorCode string, state string, region string, ) *AzureLocationHasVolumes`

NewAzureLocationHasVolumes instantiates a new AzureLocationHasVolumes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAzureLocationHasVolumesWithDefaults

`func NewAzureLocationHasVolumesWithDefaults() *AzureLocationHasVolumes`

NewAzureLocationHasVolumesWithDefaults instantiates a new AzureLocationHasVolumes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *AzureLocationHasVolumes) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *AzureLocationHasVolumes) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *AzureLocationHasVolumes) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetState

`func (o *AzureLocationHasVolumes) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *AzureLocationHasVolumes) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *AzureLocationHasVolumes) SetState(v string)`

SetState sets State field to given value.


### GetRegion

`func (o *AzureLocationHasVolumes) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *AzureLocationHasVolumes) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *AzureLocationHasVolumes) SetRegion(v string)`

SetRegion sets Region field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


