# VolumeStatusDegraded

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Msg** | **string** | Volume is degraded. One or more locations not available. | 

## Methods

### NewVolumeStatusDegraded

`func NewVolumeStatusDegraded(msg string, ) *VolumeStatusDegraded`

NewVolumeStatusDegraded instantiates a new VolumeStatusDegraded object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusDegradedWithDefaults

`func NewVolumeStatusDegradedWithDefaults() *VolumeStatusDegraded`

NewVolumeStatusDegradedWithDefaults instantiates a new VolumeStatusDegraded object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMsg

`func (o *VolumeStatusDegraded) GetMsg() string`

GetMsg returns the Msg field if non-nil, zero value otherwise.

### GetMsgOk

`func (o *VolumeStatusDegraded) GetMsgOk() (*string, bool)`

GetMsgOk returns a tuple with the Msg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMsg

`func (o *VolumeStatusDegraded) SetMsg(v string)`

SetMsg sets Msg field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


