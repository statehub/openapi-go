# StateLocationVolume

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | [**VolumeLocationStatus**](VolumeLocationStatus.md) | Volume location status | 
**Progress** | Pointer to [**VolumeLocationProgress**](VolumeLocationProgress.md) | Progress (if applicable) | [optional] 
**Name** | **string** | Volume name | 

## Methods

### NewStateLocationVolume

`func NewStateLocationVolume(status VolumeLocationStatus, name string, ) *StateLocationVolume`

NewStateLocationVolume instantiates a new StateLocationVolume object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationVolumeWithDefaults

`func NewStateLocationVolumeWithDefaults() *StateLocationVolume`

NewStateLocationVolumeWithDefaults instantiates a new StateLocationVolume object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *StateLocationVolume) GetStatus() VolumeLocationStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *StateLocationVolume) GetStatusOk() (*VolumeLocationStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *StateLocationVolume) SetStatus(v VolumeLocationStatus)`

SetStatus sets Status field to given value.


### GetProgress

`func (o *StateLocationVolume) GetProgress() VolumeLocationProgress`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *StateLocationVolume) GetProgressOk() (*VolumeLocationProgress, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *StateLocationVolume) SetProgress(v VolumeLocationProgress)`

SetProgress sets Progress field to given value.

### HasProgress

`func (o *StateLocationVolume) HasProgress() bool`

HasProgress returns a boolean if a field has been set.

### GetName

`func (o *StateLocationVolume) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *StateLocationVolume) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *StateLocationVolume) SetName(v string)`

SetName sets Name field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


