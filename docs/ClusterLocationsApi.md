# \ClusterLocationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ClusterLocationsControllerGetAllLocations**](ClusterLocationsApi.md#ClusterLocationsControllerGetAllLocations) | **Get** /v0/clusters/{cluster}/locations | Get all Location
[**ClusterLocationsControllerGetAllLocationsAws**](ClusterLocationsApi.md#ClusterLocationsControllerGetAllLocationsAws) | **Get** /v0/clusters/{cluster}/locations/aws | Get all Aws Location
[**ClusterLocationsControllerGetAllLocationsAzure**](ClusterLocationsApi.md#ClusterLocationsControllerGetAllLocationsAzure) | **Get** /v0/clusters/{cluster}/locations/azure | Get all Azure Location
[**ClusterLocationsControllerUpdateLocations**](ClusterLocationsApi.md#ClusterLocationsControllerUpdateLocations) | **Put** /v0/clusters/{cluster}/locations | Update Cluster locations



## ClusterLocationsControllerGetAllLocations

> ClusterLocations ClusterLocationsControllerGetAllLocations(ctx, cluster).Execute()

Get all Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterLocationsApi.ClusterLocationsControllerGetAllLocations(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterLocationsApi.ClusterLocationsControllerGetAllLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterLocationsControllerGetAllLocations`: ClusterLocations
    fmt.Fprintf(os.Stdout, "Response from `ClusterLocationsApi.ClusterLocationsControllerGetAllLocations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterLocationsControllerGetAllLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ClusterLocations**](ClusterLocations.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ClusterLocationsControllerGetAllLocationsAws

> []ClusterLocationAws ClusterLocationsControllerGetAllLocationsAws(ctx, cluster).Execute()

Get all Aws Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAws(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAws``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterLocationsControllerGetAllLocationsAws`: []ClusterLocationAws
    fmt.Fprintf(os.Stdout, "Response from `ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAws`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterLocationsControllerGetAllLocationsAwsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]ClusterLocationAws**](ClusterLocationAws.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ClusterLocationsControllerGetAllLocationsAzure

> []ClusterLocationAzure ClusterLocationsControllerGetAllLocationsAzure(ctx, cluster).Execute()

Get all Azure Location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAzure(context.Background(), cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAzure``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterLocationsControllerGetAllLocationsAzure`: []ClusterLocationAzure
    fmt.Fprintf(os.Stdout, "Response from `ClusterLocationsApi.ClusterLocationsControllerGetAllLocationsAzure`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterLocationsControllerGetAllLocationsAzureRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]ClusterLocationAzure**](ClusterLocationAzure.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ClusterLocationsControllerUpdateLocations

> ClusterLocationsDto ClusterLocationsControllerUpdateLocations(ctx, cluster).ClusterLocationsDto(clusterLocationsDto).Execute()

Update Cluster locations

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    cluster := "cluster_example" // string | 
    clusterLocationsDto := *openapiclient.NewClusterLocationsDto([]openapiclient.ClusterLocationAwsDto{*openapiclient.NewClusterLocationAwsDto("Region_example", "AccountPrincipal_example")}, []openapiclient.ClusterLocationAzureDto{*openapiclient.NewClusterLocationAzureDto("Region_example")}) // ClusterLocationsDto | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ClusterLocationsApi.ClusterLocationsControllerUpdateLocations(context.Background(), cluster).ClusterLocationsDto(clusterLocationsDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ClusterLocationsApi.ClusterLocationsControllerUpdateLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ClusterLocationsControllerUpdateLocations`: ClusterLocationsDto
    fmt.Fprintf(os.Stdout, "Response from `ClusterLocationsApi.ClusterLocationsControllerUpdateLocations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiClusterLocationsControllerUpdateLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **clusterLocationsDto** | [**ClusterLocationsDto**](ClusterLocationsDto.md) |  | 

### Return type

[**ClusterLocationsDto**](ClusterLocationsDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

