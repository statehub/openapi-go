# CreateStateLocationsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | Pointer to [**[]CreateStateLocationAwsDto**](CreateStateLocationAwsDto.md) | The AWS locations for the State | [optional] 
**Azure** | Pointer to [**[]CreateStateLocationAzureDto**](CreateStateLocationAzureDto.md) | The Azure locations for the State | [optional] 

## Methods

### NewCreateStateLocationsDto

`func NewCreateStateLocationsDto() *CreateStateLocationsDto`

NewCreateStateLocationsDto instantiates a new CreateStateLocationsDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStateLocationsDtoWithDefaults

`func NewCreateStateLocationsDtoWithDefaults() *CreateStateLocationsDto`

NewCreateStateLocationsDtoWithDefaults instantiates a new CreateStateLocationsDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *CreateStateLocationsDto) GetAws() []CreateStateLocationAwsDto`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *CreateStateLocationsDto) GetAwsOk() (*[]CreateStateLocationAwsDto, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *CreateStateLocationsDto) SetAws(v []CreateStateLocationAwsDto)`

SetAws sets Aws field to given value.

### HasAws

`func (o *CreateStateLocationsDto) HasAws() bool`

HasAws returns a boolean if a field has been set.

### GetAzure

`func (o *CreateStateLocationsDto) GetAzure() []CreateStateLocationAzureDto`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *CreateStateLocationsDto) GetAzureOk() (*[]CreateStateLocationAzureDto, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *CreateStateLocationsDto) SetAzure(v []CreateStateLocationAzureDto)`

SetAzure sets Azure field to given value.

### HasAzure

`func (o *CreateStateLocationsDto) HasAzure() bool`

HasAzure returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


