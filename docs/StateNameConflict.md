# StateNameConflict

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**State** | **string** |  | 

## Methods

### NewStateNameConflict

`func NewStateNameConflict(errorCode string, state string, ) *StateNameConflict`

NewStateNameConflict instantiates a new StateNameConflict object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateNameConflictWithDefaults

`func NewStateNameConflictWithDefaults() *StateNameConflict`

NewStateNameConflictWithDefaults instantiates a new StateNameConflict object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *StateNameConflict) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *StateNameConflict) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *StateNameConflict) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetState

`func (o *StateNameConflict) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *StateNameConflict) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *StateNameConflict) SetState(v string)`

SetState sets State field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


