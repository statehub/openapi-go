# ClusterTokenNotFound

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Cluster** | **string** |  | 

## Methods

### NewClusterTokenNotFound

`func NewClusterTokenNotFound(errorCode string, cluster string, ) *ClusterTokenNotFound`

NewClusterTokenNotFound instantiates a new ClusterTokenNotFound object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterTokenNotFoundWithDefaults

`func NewClusterTokenNotFoundWithDefaults() *ClusterTokenNotFound`

NewClusterTokenNotFoundWithDefaults instantiates a new ClusterTokenNotFound object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *ClusterTokenNotFound) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *ClusterTokenNotFound) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *ClusterTokenNotFound) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetCluster

`func (o *ClusterTokenNotFound) GetCluster() string`

GetCluster returns the Cluster field if non-nil, zero value otherwise.

### GetClusterOk

`func (o *ClusterTokenNotFound) GetClusterOk() (*string, bool)`

GetClusterOk returns a tuple with the Cluster field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCluster

`func (o *ClusterTokenNotFound) SetCluster(v string)`

SetCluster sets Cluster field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


