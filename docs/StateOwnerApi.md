# \StateOwnerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StateOwnerControllerSetOwner**](StateOwnerApi.md#StateOwnerControllerSetOwner) | **Put** /v0/states/{state}/owner/{cluster} | Set an owner Cluster for the State
[**StateOwnerControllerUnsetOwner**](StateOwnerApi.md#StateOwnerControllerUnsetOwner) | **Delete** /v0/states/{state}/owner | Unset the owner Cluster form the State



## StateOwnerControllerSetOwner

> State StateOwnerControllerSetOwner(ctx, state, cluster).Execute()

Set an owner Cluster for the State

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    cluster := "cluster_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateOwnerApi.StateOwnerControllerSetOwner(context.Background(), state, cluster).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateOwnerApi.StateOwnerControllerSetOwner``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateOwnerControllerSetOwner`: State
    fmt.Fprintf(os.Stdout, "Response from `StateOwnerApi.StateOwnerControllerSetOwner`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**cluster** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateOwnerControllerSetOwnerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**State**](State.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StateOwnerControllerUnsetOwner

> State StateOwnerControllerUnsetOwner(ctx, state).Execute()

Unset the owner Cluster form the State

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StateOwnerApi.StateOwnerControllerUnsetOwner(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StateOwnerApi.StateOwnerControllerUnsetOwner``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StateOwnerControllerUnsetOwner`: State
    fmt.Fprintf(os.Stdout, "Response from `StateOwnerApi.StateOwnerControllerUnsetOwner`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStateOwnerControllerUnsetOwnerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**State**](State.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

