# LocationNotFound

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Volume** | **string** |  | 
**Location** | **string** |  | 

## Methods

### NewLocationNotFound

`func NewLocationNotFound(errorCode string, volume string, location string, ) *LocationNotFound`

NewLocationNotFound instantiates a new LocationNotFound object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocationNotFoundWithDefaults

`func NewLocationNotFoundWithDefaults() *LocationNotFound`

NewLocationNotFoundWithDefaults instantiates a new LocationNotFound object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *LocationNotFound) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *LocationNotFound) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *LocationNotFound) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetVolume

`func (o *LocationNotFound) GetVolume() string`

GetVolume returns the Volume field if non-nil, zero value otherwise.

### GetVolumeOk

`func (o *LocationNotFound) GetVolumeOk() (*string, bool)`

GetVolumeOk returns a tuple with the Volume field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolume

`func (o *LocationNotFound) SetVolume(v string)`

SetVolume sets Volume field to given value.


### GetLocation

`func (o *LocationNotFound) GetLocation() string`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *LocationNotFound) GetLocationOk() (*string, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *LocationNotFound) SetLocation(v string)`

SetLocation sets Location field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


