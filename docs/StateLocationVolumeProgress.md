# StateLocationVolumeProgress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BytesSynchronized** | **float32** | Bytes synchronized | 
**BytesTotal** | **float32** | Bytes total | 

## Methods

### NewStateLocationVolumeProgress

`func NewStateLocationVolumeProgress(bytesSynchronized float32, bytesTotal float32, ) *StateLocationVolumeProgress`

NewStateLocationVolumeProgress instantiates a new StateLocationVolumeProgress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationVolumeProgressWithDefaults

`func NewStateLocationVolumeProgressWithDefaults() *StateLocationVolumeProgress`

NewStateLocationVolumeProgressWithDefaults instantiates a new StateLocationVolumeProgress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBytesSynchronized

`func (o *StateLocationVolumeProgress) GetBytesSynchronized() float32`

GetBytesSynchronized returns the BytesSynchronized field if non-nil, zero value otherwise.

### GetBytesSynchronizedOk

`func (o *StateLocationVolumeProgress) GetBytesSynchronizedOk() (*float32, bool)`

GetBytesSynchronizedOk returns a tuple with the BytesSynchronized field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBytesSynchronized

`func (o *StateLocationVolumeProgress) SetBytesSynchronized(v float32)`

SetBytesSynchronized sets BytesSynchronized field to given value.


### GetBytesTotal

`func (o *StateLocationVolumeProgress) GetBytesTotal() float32`

GetBytesTotal returns the BytesTotal field if non-nil, zero value otherwise.

### GetBytesTotalOk

`func (o *StateLocationVolumeProgress) GetBytesTotalOk() (*float32, bool)`

GetBytesTotalOk returns a tuple with the BytesTotal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBytesTotal

`func (o *StateLocationVolumeProgress) SetBytesTotal(v float32)`

SetBytesTotal sets BytesTotal field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


