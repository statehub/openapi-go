# StateLocationStatusProvisioning

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Provisioning** | **[]string** | Provisioning progress by volume | 

## Methods

### NewStateLocationStatusProvisioning

`func NewStateLocationStatusProvisioning(provisioning []string, ) *StateLocationStatusProvisioning`

NewStateLocationStatusProvisioning instantiates a new StateLocationStatusProvisioning object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationStatusProvisioningWithDefaults

`func NewStateLocationStatusProvisioningWithDefaults() *StateLocationStatusProvisioning`

NewStateLocationStatusProvisioningWithDefaults instantiates a new StateLocationStatusProvisioning object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProvisioning

`func (o *StateLocationStatusProvisioning) GetProvisioning() []string`

GetProvisioning returns the Provisioning field if non-nil, zero value otherwise.

### GetProvisioningOk

`func (o *StateLocationStatusProvisioning) GetProvisioningOk() (*[]string, bool)`

GetProvisioningOk returns a tuple with the Provisioning field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvisioning

`func (o *StateLocationStatusProvisioning) SetProvisioning(v []string)`

SetProvisioning sets Provisioning field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


