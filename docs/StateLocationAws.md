# StateLocationAws

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Region** | **string** | An AWS Region | 
**Status** | **string** | Location status | 
**Volumes** | [**[]StateLocationVolume**](StateLocationVolume.md) | Volume status in this location | 
**PrivateLinkService** | Pointer to [**StateLocationAwsPls**](StateLocationAwsPls.md) | PrivateLink Endpoint | [optional] 

## Methods

### NewStateLocationAws

`func NewStateLocationAws(region string, status string, volumes []StateLocationVolume, ) *StateLocationAws`

NewStateLocationAws instantiates a new StateLocationAws object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationAwsWithDefaults

`func NewStateLocationAwsWithDefaults() *StateLocationAws`

NewStateLocationAwsWithDefaults instantiates a new StateLocationAws object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRegion

`func (o *StateLocationAws) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *StateLocationAws) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *StateLocationAws) SetRegion(v string)`

SetRegion sets Region field to given value.


### GetStatus

`func (o *StateLocationAws) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *StateLocationAws) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *StateLocationAws) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetVolumes

`func (o *StateLocationAws) GetVolumes() []StateLocationVolume`

GetVolumes returns the Volumes field if non-nil, zero value otherwise.

### GetVolumesOk

`func (o *StateLocationAws) GetVolumesOk() (*[]StateLocationVolume, bool)`

GetVolumesOk returns a tuple with the Volumes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolumes

`func (o *StateLocationAws) SetVolumes(v []StateLocationVolume)`

SetVolumes sets Volumes field to given value.


### GetPrivateLinkService

`func (o *StateLocationAws) GetPrivateLinkService() StateLocationAwsPls`

GetPrivateLinkService returns the PrivateLinkService field if non-nil, zero value otherwise.

### GetPrivateLinkServiceOk

`func (o *StateLocationAws) GetPrivateLinkServiceOk() (*StateLocationAwsPls, bool)`

GetPrivateLinkServiceOk returns a tuple with the PrivateLinkService field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrivateLinkService

`func (o *StateLocationAws) SetPrivateLinkService(v StateLocationAwsPls)`

SetPrivateLinkService sets PrivateLinkService field to given value.

### HasPrivateLinkService

`func (o *StateLocationAws) HasPrivateLinkService() bool`

HasPrivateLinkService returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


