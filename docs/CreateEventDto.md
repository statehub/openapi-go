# CreateEventDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Reporter** | [**EventActorComponent**](EventActorComponent.md) | System component that reported an event | 
**Event** | **map[string]interface{}** | Event details | 
**Timestamp** | **time.Time** | Event timestamp | 

## Methods

### NewCreateEventDto

`func NewCreateEventDto(reporter EventActorComponent, event map[string]interface{}, timestamp time.Time, ) *CreateEventDto`

NewCreateEventDto instantiates a new CreateEventDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateEventDtoWithDefaults

`func NewCreateEventDtoWithDefaults() *CreateEventDto`

NewCreateEventDtoWithDefaults instantiates a new CreateEventDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReporter

`func (o *CreateEventDto) GetReporter() EventActorComponent`

GetReporter returns the Reporter field if non-nil, zero value otherwise.

### GetReporterOk

`func (o *CreateEventDto) GetReporterOk() (*EventActorComponent, bool)`

GetReporterOk returns a tuple with the Reporter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReporter

`func (o *CreateEventDto) SetReporter(v EventActorComponent)`

SetReporter sets Reporter field to given value.


### GetEvent

`func (o *CreateEventDto) GetEvent() map[string]interface{}`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *CreateEventDto) GetEventOk() (*map[string]interface{}, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *CreateEventDto) SetEvent(v map[string]interface{})`

SetEvent sets Event field to given value.


### GetTimestamp

`func (o *CreateEventDto) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *CreateEventDto) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *CreateEventDto) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


