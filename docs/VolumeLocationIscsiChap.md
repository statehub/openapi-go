# VolumeLocationIscsiChap

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**User** | **string** | Volume location iSCSI chap user | 
**Secret** | **string** | Volume location iSCSI chap secret | 

## Methods

### NewVolumeLocationIscsiChap

`func NewVolumeLocationIscsiChap(user string, secret string, ) *VolumeLocationIscsiChap`

NewVolumeLocationIscsiChap instantiates a new VolumeLocationIscsiChap object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeLocationIscsiChapWithDefaults

`func NewVolumeLocationIscsiChapWithDefaults() *VolumeLocationIscsiChap`

NewVolumeLocationIscsiChapWithDefaults instantiates a new VolumeLocationIscsiChap object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUser

`func (o *VolumeLocationIscsiChap) GetUser() string`

GetUser returns the User field if non-nil, zero value otherwise.

### GetUserOk

`func (o *VolumeLocationIscsiChap) GetUserOk() (*string, bool)`

GetUserOk returns a tuple with the User field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUser

`func (o *VolumeLocationIscsiChap) SetUser(v string)`

SetUser sets User field to given value.


### GetSecret

`func (o *VolumeLocationIscsiChap) GetSecret() string`

GetSecret returns the Secret field if non-nil, zero value otherwise.

### GetSecretOk

`func (o *VolumeLocationIscsiChap) GetSecretOk() (*string, bool)`

GetSecretOk returns a tuple with the Secret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecret

`func (o *VolumeLocationIscsiChap) SetSecret(v string)`

SetSecret sets Secret field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


