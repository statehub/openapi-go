# ClusterLocations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | Pointer to [**[]ClusterLocationAws**](ClusterLocationAws.md) | The AWS locations for the Cluster | [optional] 
**Azure** | Pointer to [**[]ClusterLocationAzure**](ClusterLocationAzure.md) | The Azure locations for the Cluster | [optional] 

## Methods

### NewClusterLocations

`func NewClusterLocations() *ClusterLocations`

NewClusterLocations instantiates a new ClusterLocations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterLocationsWithDefaults

`func NewClusterLocationsWithDefaults() *ClusterLocations`

NewClusterLocationsWithDefaults instantiates a new ClusterLocations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *ClusterLocations) GetAws() []ClusterLocationAws`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *ClusterLocations) GetAwsOk() (*[]ClusterLocationAws, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *ClusterLocations) SetAws(v []ClusterLocationAws)`

SetAws sets Aws field to given value.

### HasAws

`func (o *ClusterLocations) HasAws() bool`

HasAws returns a boolean if a field has been set.

### GetAzure

`func (o *ClusterLocations) GetAzure() []ClusterLocationAzure`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *ClusterLocations) GetAzureOk() (*[]ClusterLocationAzure, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *ClusterLocations) SetAzure(v []ClusterLocationAzure)`

SetAzure sets Azure field to given value.

### HasAzure

`func (o *ClusterLocations) HasAzure() bool`

HasAzure returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


