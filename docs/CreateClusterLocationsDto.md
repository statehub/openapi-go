# CreateClusterLocationsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | Pointer to [**[]CreateClusterLocationAwsDto**](CreateClusterLocationAwsDto.md) | The AWS locations for the Cluster | [optional] 
**Azure** | Pointer to [**[]CreateClusterLocationAzureDto**](CreateClusterLocationAzureDto.md) | The Azure locations for the Cluster | [optional] 

## Methods

### NewCreateClusterLocationsDto

`func NewCreateClusterLocationsDto() *CreateClusterLocationsDto`

NewCreateClusterLocationsDto instantiates a new CreateClusterLocationsDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateClusterLocationsDtoWithDefaults

`func NewCreateClusterLocationsDtoWithDefaults() *CreateClusterLocationsDto`

NewCreateClusterLocationsDtoWithDefaults instantiates a new CreateClusterLocationsDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *CreateClusterLocationsDto) GetAws() []CreateClusterLocationAwsDto`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *CreateClusterLocationsDto) GetAwsOk() (*[]CreateClusterLocationAwsDto, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *CreateClusterLocationsDto) SetAws(v []CreateClusterLocationAwsDto)`

SetAws sets Aws field to given value.

### HasAws

`func (o *CreateClusterLocationsDto) HasAws() bool`

HasAws returns a boolean if a field has been set.

### GetAzure

`func (o *CreateClusterLocationsDto) GetAzure() []CreateClusterLocationAzureDto`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *CreateClusterLocationsDto) GetAzureOk() (*[]CreateClusterLocationAzureDto, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *CreateClusterLocationsDto) SetAzure(v []CreateClusterLocationAzureDto)`

SetAzure sets Azure field to given value.

### HasAzure

`func (o *CreateClusterLocationsDto) HasAzure() bool`

HasAzure returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


