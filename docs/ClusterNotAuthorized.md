# ClusterNotAuthorized

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Permission** | **string** |  | 
**ResourceType** | **string** |  | 
**ResourceName** | **string** |  | 

## Methods

### NewClusterNotAuthorized

`func NewClusterNotAuthorized(errorCode string, permission string, resourceType string, resourceName string, ) *ClusterNotAuthorized`

NewClusterNotAuthorized instantiates a new ClusterNotAuthorized object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterNotAuthorizedWithDefaults

`func NewClusterNotAuthorizedWithDefaults() *ClusterNotAuthorized`

NewClusterNotAuthorizedWithDefaults instantiates a new ClusterNotAuthorized object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *ClusterNotAuthorized) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *ClusterNotAuthorized) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *ClusterNotAuthorized) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetPermission

`func (o *ClusterNotAuthorized) GetPermission() string`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *ClusterNotAuthorized) GetPermissionOk() (*string, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *ClusterNotAuthorized) SetPermission(v string)`

SetPermission sets Permission field to given value.


### GetResourceType

`func (o *ClusterNotAuthorized) GetResourceType() string`

GetResourceType returns the ResourceType field if non-nil, zero value otherwise.

### GetResourceTypeOk

`func (o *ClusterNotAuthorized) GetResourceTypeOk() (*string, bool)`

GetResourceTypeOk returns a tuple with the ResourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceType

`func (o *ClusterNotAuthorized) SetResourceType(v string)`

SetResourceType sets ResourceType field to given value.


### GetResourceName

`func (o *ClusterNotAuthorized) GetResourceName() string`

GetResourceName returns the ResourceName field if non-nil, zero value otherwise.

### GetResourceNameOk

`func (o *ClusterNotAuthorized) GetResourceNameOk() (*string, bool)`

GetResourceNameOk returns a tuple with the ResourceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceName

`func (o *ClusterNotAuthorized) SetResourceName(v string)`

SetResourceName sets ResourceName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


