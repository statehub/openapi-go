# VolumeSizeNotSupported

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Size** | **float32** |  | 

## Methods

### NewVolumeSizeNotSupported

`func NewVolumeSizeNotSupported(errorCode string, size float32, ) *VolumeSizeNotSupported`

NewVolumeSizeNotSupported instantiates a new VolumeSizeNotSupported object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeSizeNotSupportedWithDefaults

`func NewVolumeSizeNotSupportedWithDefaults() *VolumeSizeNotSupported`

NewVolumeSizeNotSupportedWithDefaults instantiates a new VolumeSizeNotSupported object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *VolumeSizeNotSupported) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *VolumeSizeNotSupported) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *VolumeSizeNotSupported) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetSize

`func (o *VolumeSizeNotSupported) GetSize() float32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *VolumeSizeNotSupported) GetSizeOk() (*float32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *VolumeSizeNotSupported) SetSize(v float32)`

SetSize sets Size field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


