# FreeTierLimitExceeded

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Type** | **string** |  | 
**NLimit** | **float32** |  | 
**StrLimit** | **string** |  | 

## Methods

### NewFreeTierLimitExceeded

`func NewFreeTierLimitExceeded(errorCode string, type_ string, nLimit float32, strLimit string, ) *FreeTierLimitExceeded`

NewFreeTierLimitExceeded instantiates a new FreeTierLimitExceeded object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFreeTierLimitExceededWithDefaults

`func NewFreeTierLimitExceededWithDefaults() *FreeTierLimitExceeded`

NewFreeTierLimitExceededWithDefaults instantiates a new FreeTierLimitExceeded object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *FreeTierLimitExceeded) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *FreeTierLimitExceeded) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *FreeTierLimitExceeded) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetType

`func (o *FreeTierLimitExceeded) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *FreeTierLimitExceeded) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *FreeTierLimitExceeded) SetType(v string)`

SetType sets Type field to given value.


### GetNLimit

`func (o *FreeTierLimitExceeded) GetNLimit() float32`

GetNLimit returns the NLimit field if non-nil, zero value otherwise.

### GetNLimitOk

`func (o *FreeTierLimitExceeded) GetNLimitOk() (*float32, bool)`

GetNLimitOk returns a tuple with the NLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNLimit

`func (o *FreeTierLimitExceeded) SetNLimit(v float32)`

SetNLimit sets NLimit field to given value.


### GetStrLimit

`func (o *FreeTierLimitExceeded) GetStrLimit() string`

GetStrLimit returns the StrLimit field if non-nil, zero value otherwise.

### GetStrLimitOk

`func (o *FreeTierLimitExceeded) GetStrLimitOk() (*string, bool)`

GetStrLimitOk returns a tuple with the StrLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrLimit

`func (o *FreeTierLimitExceeded) SetStrLimit(v string)`

SetStrLimit sets StrLimit field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


