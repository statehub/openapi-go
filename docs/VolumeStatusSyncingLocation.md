# VolumeStatusSyncingLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Location** | **string** | The location in which the volume is synchronizing. Format: &#x60;{vendor}/{region}&#x60; (e.g. &#x60;aws/us-east-1&#x60;) | 
**BytesSynchronized** | **float32** | Bytes synchronized | 
**BytesTotal** | **float32** | Bytes total | 

## Methods

### NewVolumeStatusSyncingLocation

`func NewVolumeStatusSyncingLocation(location string, bytesSynchronized float32, bytesTotal float32, ) *VolumeStatusSyncingLocation`

NewVolumeStatusSyncingLocation instantiates a new VolumeStatusSyncingLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVolumeStatusSyncingLocationWithDefaults

`func NewVolumeStatusSyncingLocationWithDefaults() *VolumeStatusSyncingLocation`

NewVolumeStatusSyncingLocationWithDefaults instantiates a new VolumeStatusSyncingLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocation

`func (o *VolumeStatusSyncingLocation) GetLocation() string`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *VolumeStatusSyncingLocation) GetLocationOk() (*string, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *VolumeStatusSyncingLocation) SetLocation(v string)`

SetLocation sets Location field to given value.


### GetBytesSynchronized

`func (o *VolumeStatusSyncingLocation) GetBytesSynchronized() float32`

GetBytesSynchronized returns the BytesSynchronized field if non-nil, zero value otherwise.

### GetBytesSynchronizedOk

`func (o *VolumeStatusSyncingLocation) GetBytesSynchronizedOk() (*float32, bool)`

GetBytesSynchronizedOk returns a tuple with the BytesSynchronized field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBytesSynchronized

`func (o *VolumeStatusSyncingLocation) SetBytesSynchronized(v float32)`

SetBytesSynchronized sets BytesSynchronized field to given value.


### GetBytesTotal

`func (o *VolumeStatusSyncingLocation) GetBytesTotal() float32`

GetBytesTotal returns the BytesTotal field if non-nil, zero value otherwise.

### GetBytesTotalOk

`func (o *VolumeStatusSyncingLocation) GetBytesTotalOk() (*float32, bool)`

GetBytesTotalOk returns a tuple with the BytesTotal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBytesTotal

`func (o *VolumeStatusSyncingLocation) SetBytesTotal(v float32)`

SetBytesTotal sets BytesTotal field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


