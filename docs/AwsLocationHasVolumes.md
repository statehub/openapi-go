# AwsLocationHasVolumes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**State** | **string** |  | 
**Region** | **string** |  | 

## Methods

### NewAwsLocationHasVolumes

`func NewAwsLocationHasVolumes(errorCode string, state string, region string, ) *AwsLocationHasVolumes`

NewAwsLocationHasVolumes instantiates a new AwsLocationHasVolumes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAwsLocationHasVolumesWithDefaults

`func NewAwsLocationHasVolumesWithDefaults() *AwsLocationHasVolumes`

NewAwsLocationHasVolumesWithDefaults instantiates a new AwsLocationHasVolumes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *AwsLocationHasVolumes) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *AwsLocationHasVolumes) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *AwsLocationHasVolumes) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetState

`func (o *AwsLocationHasVolumes) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *AwsLocationHasVolumes) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *AwsLocationHasVolumes) SetState(v string)`

SetState sets State field to given value.


### GetRegion

`func (o *AwsLocationHasVolumes) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *AwsLocationHasVolumes) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *AwsLocationHasVolumes) SetRegion(v string)`

SetRegion sets Region field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


