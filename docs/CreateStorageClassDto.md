# CreateStorageClassDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Storage class name | 
**VolumeBindingMode** | Pointer to **string** | The &#x60;volumeBindingMode&#x60; field of the StorageClass | [optional] [default to "WaitForFirstConsumer"]
**MountOptions** | Pointer to **string** | The &#x60;mountOptions&#x60; field of the StorageClass | [optional] 
**FsType** | Pointer to **string** | The file system type of the state | [optional] [default to "ext4"]

## Methods

### NewCreateStorageClassDto

`func NewCreateStorageClassDto(name string, ) *CreateStorageClassDto`

NewCreateStorageClassDto instantiates a new CreateStorageClassDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStorageClassDtoWithDefaults

`func NewCreateStorageClassDtoWithDefaults() *CreateStorageClassDto`

NewCreateStorageClassDtoWithDefaults instantiates a new CreateStorageClassDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateStorageClassDto) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateStorageClassDto) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateStorageClassDto) SetName(v string)`

SetName sets Name field to given value.


### GetVolumeBindingMode

`func (o *CreateStorageClassDto) GetVolumeBindingMode() string`

GetVolumeBindingMode returns the VolumeBindingMode field if non-nil, zero value otherwise.

### GetVolumeBindingModeOk

`func (o *CreateStorageClassDto) GetVolumeBindingModeOk() (*string, bool)`

GetVolumeBindingModeOk returns a tuple with the VolumeBindingMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVolumeBindingMode

`func (o *CreateStorageClassDto) SetVolumeBindingMode(v string)`

SetVolumeBindingMode sets VolumeBindingMode field to given value.

### HasVolumeBindingMode

`func (o *CreateStorageClassDto) HasVolumeBindingMode() bool`

HasVolumeBindingMode returns a boolean if a field has been set.

### GetMountOptions

`func (o *CreateStorageClassDto) GetMountOptions() string`

GetMountOptions returns the MountOptions field if non-nil, zero value otherwise.

### GetMountOptionsOk

`func (o *CreateStorageClassDto) GetMountOptionsOk() (*string, bool)`

GetMountOptionsOk returns a tuple with the MountOptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMountOptions

`func (o *CreateStorageClassDto) SetMountOptions(v string)`

SetMountOptions sets MountOptions field to given value.

### HasMountOptions

`func (o *CreateStorageClassDto) HasMountOptions() bool`

HasMountOptions returns a boolean if a field has been set.

### GetFsType

`func (o *CreateStorageClassDto) GetFsType() string`

GetFsType returns the FsType field if non-nil, zero value otherwise.

### GetFsTypeOk

`func (o *CreateStorageClassDto) GetFsTypeOk() (*string, bool)`

GetFsTypeOk returns a tuple with the FsType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFsType

`func (o *CreateStorageClassDto) SetFsType(v string)`

SetFsType sets FsType field to given value.

### HasFsType

`func (o *CreateStorageClassDto) HasFsType() bool`

HasFsType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


