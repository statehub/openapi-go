# \VolumeActiveLocationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VolumeActiveLocationControllerSet**](VolumeActiveLocationApi.md#VolumeActiveLocationControllerSet) | **Put** /v0/states/{state}/volumes/{volume}/activeLocation/{cloud}/{region} | Set the active location for the Volume
[**VolumeActiveLocationControllerUnset**](VolumeActiveLocationApi.md#VolumeActiveLocationControllerUnset) | **Delete** /v0/states/{state}/volumes/{volume}/activeLocation | Unset the owner node from the Volume



## VolumeActiveLocationControllerSet

> Volume VolumeActiveLocationControllerSet(ctx, state, volume, cloud, region).Execute()

Set the active location for the Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    volume := "volume_example" // string | 
    cloud := "cloud_example" // string | 
    region := "region_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumeActiveLocationApi.VolumeActiveLocationControllerSet(context.Background(), state, volume, cloud, region).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumeActiveLocationApi.VolumeActiveLocationControllerSet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumeActiveLocationControllerSet`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumeActiveLocationApi.VolumeActiveLocationControllerSet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**volume** | **string** |  | 
**cloud** | **string** |  | 
**region** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumeActiveLocationControllerSetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VolumeActiveLocationControllerUnset

> Volume VolumeActiveLocationControllerUnset(ctx, state, volume).Execute()

Unset the owner node from the Volume

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    volume := "volume_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.VolumeActiveLocationApi.VolumeActiveLocationControllerUnset(context.Background(), state, volume).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VolumeActiveLocationApi.VolumeActiveLocationControllerUnset``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `VolumeActiveLocationControllerUnset`: Volume
    fmt.Fprintf(os.Stdout, "Response from `VolumeActiveLocationApi.VolumeActiveLocationControllerUnset`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 
**volume** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiVolumeActiveLocationControllerUnsetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Volume**](Volume.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

