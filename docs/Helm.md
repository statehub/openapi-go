# Helm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Repo** | **string** | Helm  description | 
**Chart** | **string** | Helm chart | 
**Version** | **string** | Helm version | 
**Parameters** | **map[string]interface{}** | Helm parameters | 
**Modified** | **time.Time** | Modify date | 

## Methods

### NewHelm

`func NewHelm(repo string, chart string, version string, parameters map[string]interface{}, modified time.Time, ) *Helm`

NewHelm instantiates a new Helm object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHelmWithDefaults

`func NewHelmWithDefaults() *Helm`

NewHelmWithDefaults instantiates a new Helm object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRepo

`func (o *Helm) GetRepo() string`

GetRepo returns the Repo field if non-nil, zero value otherwise.

### GetRepoOk

`func (o *Helm) GetRepoOk() (*string, bool)`

GetRepoOk returns a tuple with the Repo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRepo

`func (o *Helm) SetRepo(v string)`

SetRepo sets Repo field to given value.


### GetChart

`func (o *Helm) GetChart() string`

GetChart returns the Chart field if non-nil, zero value otherwise.

### GetChartOk

`func (o *Helm) GetChartOk() (*string, bool)`

GetChartOk returns a tuple with the Chart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChart

`func (o *Helm) SetChart(v string)`

SetChart sets Chart field to given value.


### GetVersion

`func (o *Helm) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *Helm) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *Helm) SetVersion(v string)`

SetVersion sets Version field to given value.


### GetParameters

`func (o *Helm) GetParameters() map[string]interface{}`

GetParameters returns the Parameters field if non-nil, zero value otherwise.

### GetParametersOk

`func (o *Helm) GetParametersOk() (*map[string]interface{}, bool)`

GetParametersOk returns a tuple with the Parameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParameters

`func (o *Helm) SetParameters(v map[string]interface{})`

SetParameters sets Parameters field to given value.


### GetModified

`func (o *Helm) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *Helm) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *Helm) SetModified(v time.Time)`

SetModified sets Modified field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


