# CreateStateDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the State | 
**StorageClass** | Pointer to [**CreateStorageClassDto**](CreateStorageClassDto.md) | The values to be passed to the StorageClass | [optional] 
**Locations** | Pointer to [**CreateStateLocationsDto**](CreateStateLocationsDto.md) | The Locations of the State | [optional] 
**Owner** | Pointer to **string** | The Cluster that currently owns the State | [optional] 

## Methods

### NewCreateStateDto

`func NewCreateStateDto(name string, ) *CreateStateDto`

NewCreateStateDto instantiates a new CreateStateDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateStateDtoWithDefaults

`func NewCreateStateDtoWithDefaults() *CreateStateDto`

NewCreateStateDtoWithDefaults instantiates a new CreateStateDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateStateDto) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateStateDto) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateStateDto) SetName(v string)`

SetName sets Name field to given value.


### GetStorageClass

`func (o *CreateStateDto) GetStorageClass() CreateStorageClassDto`

GetStorageClass returns the StorageClass field if non-nil, zero value otherwise.

### GetStorageClassOk

`func (o *CreateStateDto) GetStorageClassOk() (*CreateStorageClassDto, bool)`

GetStorageClassOk returns a tuple with the StorageClass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageClass

`func (o *CreateStateDto) SetStorageClass(v CreateStorageClassDto)`

SetStorageClass sets StorageClass field to given value.

### HasStorageClass

`func (o *CreateStateDto) HasStorageClass() bool`

HasStorageClass returns a boolean if a field has been set.

### GetLocations

`func (o *CreateStateDto) GetLocations() CreateStateLocationsDto`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *CreateStateDto) GetLocationsOk() (*CreateStateLocationsDto, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *CreateStateDto) SetLocations(v CreateStateLocationsDto)`

SetLocations sets Locations field to given value.

### HasLocations

`func (o *CreateStateDto) HasLocations() bool`

HasLocations returns a boolean if a field has been set.

### GetOwner

`func (o *CreateStateDto) GetOwner() string`

GetOwner returns the Owner field if non-nil, zero value otherwise.

### GetOwnerOk

`func (o *CreateStateDto) GetOwnerOk() (*string, bool)`

GetOwnerOk returns a tuple with the Owner field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwner

`func (o *CreateStateDto) SetOwner(v string)`

SetOwner sets Owner field to given value.

### HasOwner

`func (o *CreateStateDto) HasOwner() bool`

HasOwner returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


