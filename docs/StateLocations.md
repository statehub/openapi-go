# StateLocations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aws** | Pointer to [**[]StateLocationAws**](StateLocationAws.md) | The AWS locations for the State | [optional] 
**Azure** | Pointer to [**[]StateLocationAzure**](StateLocationAzure.md) | The Azure locations for the State | [optional] 

## Methods

### NewStateLocations

`func NewStateLocations() *StateLocations`

NewStateLocations instantiates a new StateLocations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationsWithDefaults

`func NewStateLocationsWithDefaults() *StateLocations`

NewStateLocationsWithDefaults instantiates a new StateLocations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAws

`func (o *StateLocations) GetAws() []StateLocationAws`

GetAws returns the Aws field if non-nil, zero value otherwise.

### GetAwsOk

`func (o *StateLocations) GetAwsOk() (*[]StateLocationAws, bool)`

GetAwsOk returns a tuple with the Aws field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAws

`func (o *StateLocations) SetAws(v []StateLocationAws)`

SetAws sets Aws field to given value.

### HasAws

`func (o *StateLocations) HasAws() bool`

HasAws returns a boolean if a field has been set.

### GetAzure

`func (o *StateLocations) GetAzure() []StateLocationAzure`

GetAzure returns the Azure field if non-nil, zero value otherwise.

### GetAzureOk

`func (o *StateLocations) GetAzureOk() (*[]StateLocationAzure, bool)`

GetAzureOk returns a tuple with the Azure field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAzure

`func (o *StateLocations) SetAzure(v []StateLocationAzure)`

SetAzure sets Azure field to given value.

### HasAzure

`func (o *StateLocations) HasAzure() bool`

HasAzure returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


