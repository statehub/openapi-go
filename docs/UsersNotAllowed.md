# UsersNotAllowed

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 

## Methods

### NewUsersNotAllowed

`func NewUsersNotAllowed(errorCode string, ) *UsersNotAllowed`

NewUsersNotAllowed instantiates a new UsersNotAllowed object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUsersNotAllowedWithDefaults

`func NewUsersNotAllowedWithDefaults() *UsersNotAllowed`

NewUsersNotAllowedWithDefaults instantiates a new UsersNotAllowed object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *UsersNotAllowed) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *UsersNotAllowed) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *UsersNotAllowed) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


