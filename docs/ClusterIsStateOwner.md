# ClusterIsStateOwner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ErrorCode** | **string** |  | 
**Cluster** | **string** |  | 
**State** | **string** |  | 

## Methods

### NewClusterIsStateOwner

`func NewClusterIsStateOwner(errorCode string, cluster string, state string, ) *ClusterIsStateOwner`

NewClusterIsStateOwner instantiates a new ClusterIsStateOwner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterIsStateOwnerWithDefaults

`func NewClusterIsStateOwnerWithDefaults() *ClusterIsStateOwner`

NewClusterIsStateOwnerWithDefaults instantiates a new ClusterIsStateOwner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetErrorCode

`func (o *ClusterIsStateOwner) GetErrorCode() string`

GetErrorCode returns the ErrorCode field if non-nil, zero value otherwise.

### GetErrorCodeOk

`func (o *ClusterIsStateOwner) GetErrorCodeOk() (*string, bool)`

GetErrorCodeOk returns a tuple with the ErrorCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorCode

`func (o *ClusterIsStateOwner) SetErrorCode(v string)`

SetErrorCode sets ErrorCode field to given value.


### GetCluster

`func (o *ClusterIsStateOwner) GetCluster() string`

GetCluster returns the Cluster field if non-nil, zero value otherwise.

### GetClusterOk

`func (o *ClusterIsStateOwner) GetClusterOk() (*string, bool)`

GetClusterOk returns a tuple with the Cluster field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCluster

`func (o *ClusterIsStateOwner) SetCluster(v string)`

SetCluster sets Cluster field to given value.


### GetState

`func (o *ClusterIsStateOwner) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *ClusterIsStateOwner) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *ClusterIsStateOwner) SetState(v string)`

SetState sets State field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


