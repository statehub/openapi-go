# StateLocationStatusRecovering

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Recovering** | **[]string** | Recovery progress by volume | 

## Methods

### NewStateLocationStatusRecovering

`func NewStateLocationStatusRecovering(recovering []string, ) *StateLocationStatusRecovering`

NewStateLocationStatusRecovering instantiates a new StateLocationStatusRecovering object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateLocationStatusRecoveringWithDefaults

`func NewStateLocationStatusRecoveringWithDefaults() *StateLocationStatusRecovering`

NewStateLocationStatusRecoveringWithDefaults instantiates a new StateLocationStatusRecovering object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRecovering

`func (o *StateLocationStatusRecovering) GetRecovering() []string`

GetRecovering returns the Recovering field if non-nil, zero value otherwise.

### GetRecoveringOk

`func (o *StateLocationStatusRecovering) GetRecoveringOk() (*[]string, bool)`

GetRecoveringOk returns a tuple with the Recovering field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecovering

`func (o *StateLocationStatusRecovering) SetRecovering(v []string)`

SetRecovering sets Recovering field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


