# Cluster

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The unique ID of object | 
**Name** | **string** | The name of the Cluster | 
**Namespace** | Pointer to **string** | Cluster namespace  | [optional] 
**Provider** | **string** | Cluster provider | [default to "generic"]
**Created** | **time.Time** | Creation timestamp | 
**Modified** | **time.Time** | Modified timestamp | 
**Pinged** | Pointer to **time.Time** | Modified timestamp | [optional] 
**Locations** | Pointer to [**ClusterLocations**](ClusterLocations.md) | Cluster locations | [optional] 
**Helm** | [**[]Helm**](Helm.md) | Cluster helm charts | 

## Methods

### NewCluster

`func NewCluster(id string, name string, provider string, created time.Time, modified time.Time, helm []Helm, ) *Cluster`

NewCluster instantiates a new Cluster object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClusterWithDefaults

`func NewClusterWithDefaults() *Cluster`

NewClusterWithDefaults instantiates a new Cluster object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Cluster) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Cluster) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Cluster) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *Cluster) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Cluster) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Cluster) SetName(v string)`

SetName sets Name field to given value.


### GetNamespace

`func (o *Cluster) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *Cluster) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *Cluster) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.

### HasNamespace

`func (o *Cluster) HasNamespace() bool`

HasNamespace returns a boolean if a field has been set.

### GetProvider

`func (o *Cluster) GetProvider() string`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *Cluster) GetProviderOk() (*string, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *Cluster) SetProvider(v string)`

SetProvider sets Provider field to given value.


### GetCreated

`func (o *Cluster) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *Cluster) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *Cluster) SetCreated(v time.Time)`

SetCreated sets Created field to given value.


### GetModified

`func (o *Cluster) GetModified() time.Time`

GetModified returns the Modified field if non-nil, zero value otherwise.

### GetModifiedOk

`func (o *Cluster) GetModifiedOk() (*time.Time, bool)`

GetModifiedOk returns a tuple with the Modified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModified

`func (o *Cluster) SetModified(v time.Time)`

SetModified sets Modified field to given value.


### GetPinged

`func (o *Cluster) GetPinged() time.Time`

GetPinged returns the Pinged field if non-nil, zero value otherwise.

### GetPingedOk

`func (o *Cluster) GetPingedOk() (*time.Time, bool)`

GetPingedOk returns a tuple with the Pinged field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPinged

`func (o *Cluster) SetPinged(v time.Time)`

SetPinged sets Pinged field to given value.

### HasPinged

`func (o *Cluster) HasPinged() bool`

HasPinged returns a boolean if a field has been set.

### GetLocations

`func (o *Cluster) GetLocations() ClusterLocations`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *Cluster) GetLocationsOk() (*ClusterLocations, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *Cluster) SetLocations(v ClusterLocations)`

SetLocations sets Locations field to given value.

### HasLocations

`func (o *Cluster) HasLocations() bool`

HasLocations returns a boolean if a field has been set.

### GetHelm

`func (o *Cluster) GetHelm() []Helm`

GetHelm returns the Helm field if non-nil, zero value otherwise.

### GetHelmOk

`func (o *Cluster) GetHelmOk() (*[]Helm, bool)`

GetHelmOk returns a tuple with the Helm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHelm

`func (o *Cluster) SetHelm(v []Helm)`

SetHelm sets Helm field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


