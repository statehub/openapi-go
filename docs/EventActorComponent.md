# EventActorComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | Component name | [optional] 
**Version** | Pointer to **string** | Component version | [optional] 

## Methods

### NewEventActorComponent

`func NewEventActorComponent() *EventActorComponent`

NewEventActorComponent instantiates a new EventActorComponent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEventActorComponentWithDefaults

`func NewEventActorComponentWithDefaults() *EventActorComponent`

NewEventActorComponentWithDefaults instantiates a new EventActorComponent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *EventActorComponent) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *EventActorComponent) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *EventActorComponent) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *EventActorComponent) HasName() bool`

HasName returns a boolean if a field has been set.

### GetVersion

`func (o *EventActorComponent) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *EventActorComponent) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *EventActorComponent) SetVersion(v string)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *EventActorComponent) HasVersion() bool`

HasVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


