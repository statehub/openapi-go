# \StatesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StatesControllerCreate**](StatesApi.md#StatesControllerCreate) | **Post** /v0/states | Create a State
[**StatesControllerDelete**](StatesApi.md#StatesControllerDelete) | **Delete** /v0/states/{state} | Delete a State
[**StatesControllerFindMany**](StatesApi.md#StatesControllerFindMany) | **Get** /v0/states | Get all States
[**StatesControllerFindOne**](StatesApi.md#StatesControllerFindOne) | **Get** /v0/states/{state} | Get a State



## StatesControllerCreate

> State StatesControllerCreate(ctx).CreateStateDto(createStateDto).Execute()

Create a State

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createStateDto := *openapiclient.NewCreateStateDto("online-store") // CreateStateDto | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StatesApi.StatesControllerCreate(context.Background()).CreateStateDto(createStateDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StatesApi.StatesControllerCreate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StatesControllerCreate`: State
    fmt.Fprintf(os.Stdout, "Response from `StatesApi.StatesControllerCreate`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStatesControllerCreateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createStateDto** | [**CreateStateDto**](CreateStateDto.md) |  | 

### Return type

[**State**](State.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StatesControllerDelete

> StatesControllerDelete(ctx, state).Force(force).Execute()

Delete a State

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 
    force := true // bool |  (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StatesApi.StatesControllerDelete(context.Background(), state).Force(force).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StatesApi.StatesControllerDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStatesControllerDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **force** | **bool** |  | [default to false]

### Return type

 (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StatesControllerFindMany

> []State StatesControllerFindMany(ctx).Execute()

Get all States

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StatesApi.StatesControllerFindMany(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StatesApi.StatesControllerFindMany``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StatesControllerFindMany`: []State
    fmt.Fprintf(os.Stdout, "Response from `StatesApi.StatesControllerFindMany`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiStatesControllerFindManyRequest struct via the builder pattern


### Return type

[**[]State**](State.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StatesControllerFindOne

> State StatesControllerFindOne(ctx, state).Execute()

Get a State

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    state := "state_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.StatesApi.StatesControllerFindOne(context.Background(), state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `StatesApi.StatesControllerFindOne``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StatesControllerFindOne`: State
    fmt.Fprintf(os.Stdout, "Response from `StatesApi.StatesControllerFindOne`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**state** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStatesControllerFindOneRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**State**](State.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

