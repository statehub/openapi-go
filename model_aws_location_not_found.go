/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: v0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AwsLocationNotFound struct for AwsLocationNotFound
type AwsLocationNotFound struct {
	ErrorCode string `json:"errorCode"`
	State string `json:"state"`
	Region string `json:"region"`
}

// NewAwsLocationNotFound instantiates a new AwsLocationNotFound object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAwsLocationNotFound(errorCode string, state string, region string) *AwsLocationNotFound {
	this := AwsLocationNotFound{}
	this.ErrorCode = errorCode
	this.State = state
	this.Region = region
	return &this
}

// NewAwsLocationNotFoundWithDefaults instantiates a new AwsLocationNotFound object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAwsLocationNotFoundWithDefaults() *AwsLocationNotFound {
	this := AwsLocationNotFound{}
	return &this
}

// GetErrorCode returns the ErrorCode field value
func (o *AwsLocationNotFound) GetErrorCode() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ErrorCode
}

// GetErrorCodeOk returns a tuple with the ErrorCode field value
// and a boolean to check if the value has been set.
func (o *AwsLocationNotFound) GetErrorCodeOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ErrorCode, true
}

// SetErrorCode sets field value
func (o *AwsLocationNotFound) SetErrorCode(v string) {
	o.ErrorCode = v
}

// GetState returns the State field value
func (o *AwsLocationNotFound) GetState() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.State
}

// GetStateOk returns a tuple with the State field value
// and a boolean to check if the value has been set.
func (o *AwsLocationNotFound) GetStateOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.State, true
}

// SetState sets field value
func (o *AwsLocationNotFound) SetState(v string) {
	o.State = v
}

// GetRegion returns the Region field value
func (o *AwsLocationNotFound) GetRegion() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Region
}

// GetRegionOk returns a tuple with the Region field value
// and a boolean to check if the value has been set.
func (o *AwsLocationNotFound) GetRegionOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Region, true
}

// SetRegion sets field value
func (o *AwsLocationNotFound) SetRegion(v string) {
	o.Region = v
}

func (o AwsLocationNotFound) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["errorCode"] = o.ErrorCode
	}
	if true {
		toSerialize["state"] = o.State
	}
	if true {
		toSerialize["region"] = o.Region
	}
	return json.Marshal(toSerialize)
}

type NullableAwsLocationNotFound struct {
	value *AwsLocationNotFound
	isSet bool
}

func (v NullableAwsLocationNotFound) Get() *AwsLocationNotFound {
	return v.value
}

func (v *NullableAwsLocationNotFound) Set(val *AwsLocationNotFound) {
	v.value = val
	v.isSet = true
}

func (v NullableAwsLocationNotFound) IsSet() bool {
	return v.isSet
}

func (v *NullableAwsLocationNotFound) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAwsLocationNotFound(val *AwsLocationNotFound) *NullableAwsLocationNotFound {
	return &NullableAwsLocationNotFound{value: val, isSet: true}
}

func (v NullableAwsLocationNotFound) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAwsLocationNotFound) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


