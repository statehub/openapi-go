spec:
    curl -Ss https://api.dev.statehub.io/v0/api-json -o ./spec.json

build:
    docker run --network host --rm -v "${PWD}:/local" openapitools/openapi-generator-cli:v5.1.0 generate -i /local/spec.json -g go -o /local

cleanup: 
    git checkout .gitignore go.mod go.sum

all: spec build cleanup