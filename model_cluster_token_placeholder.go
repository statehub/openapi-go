/*
 * Statehub API
 *
 * Statehub API description
 *
 * API version: v0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ClusterTokenPlaceholder struct for ClusterTokenPlaceholder
type ClusterTokenPlaceholder struct {
	// A placeholder for the Cluster's Token ('xxx')
	Token string `json:"token"`
}

// NewClusterTokenPlaceholder instantiates a new ClusterTokenPlaceholder object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewClusterTokenPlaceholder(token string) *ClusterTokenPlaceholder {
	this := ClusterTokenPlaceholder{}
	this.Token = token
	return &this
}

// NewClusterTokenPlaceholderWithDefaults instantiates a new ClusterTokenPlaceholder object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewClusterTokenPlaceholderWithDefaults() *ClusterTokenPlaceholder {
	this := ClusterTokenPlaceholder{}
	return &this
}

// GetToken returns the Token field value
func (o *ClusterTokenPlaceholder) GetToken() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Token
}

// GetTokenOk returns a tuple with the Token field value
// and a boolean to check if the value has been set.
func (o *ClusterTokenPlaceholder) GetTokenOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Token, true
}

// SetToken sets field value
func (o *ClusterTokenPlaceholder) SetToken(v string) {
	o.Token = v
}

func (o ClusterTokenPlaceholder) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["token"] = o.Token
	}
	return json.Marshal(toSerialize)
}

type NullableClusterTokenPlaceholder struct {
	value *ClusterTokenPlaceholder
	isSet bool
}

func (v NullableClusterTokenPlaceholder) Get() *ClusterTokenPlaceholder {
	return v.value
}

func (v *NullableClusterTokenPlaceholder) Set(val *ClusterTokenPlaceholder) {
	v.value = val
	v.isSet = true
}

func (v NullableClusterTokenPlaceholder) IsSet() bool {
	return v.isSet
}

func (v *NullableClusterTokenPlaceholder) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableClusterTokenPlaceholder(val *ClusterTokenPlaceholder) *NullableClusterTokenPlaceholder {
	return &NullableClusterTokenPlaceholder{value: val, isSet: true}
}

func (v NullableClusterTokenPlaceholder) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableClusterTokenPlaceholder) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


