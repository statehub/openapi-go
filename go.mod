module gitlab.com/statehub/openapi-go
go 1.13

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/oauth2 v0.0.0-20210218202405-ba52d332ba99
	google.golang.org/appengine v1.6.7 // indirect
)
